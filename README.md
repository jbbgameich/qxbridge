<!--
SPDX-FileCopyrightText: 2021 Linus Jahn <lnj@kaidan.im>

SPDX-License-Identifier: CC0-1.0
-->

# qxbridge

QXmpp based XMPP/Telegram bridge.
