// SPDX-FileCopyrightText: 2021 Linus Jahn <lnj@kaidan.im>
//
// SPDX-License-Identifier: AGPL-3.0-only

#pragma once

#include <memory>
#include <QObject>

namespace QTgBot {
class Message;
}

class Database;
class TgBotClient;
class XmppService;
class QXmppMessage;
class ChatUser;
class BridgeControllerPrivate;

class BridgeController : public QObject
{
    Q_OBJECT

public:
    explicit BridgeController(QObject *parent = nullptr);
    ~BridgeController();

private:
    Q_SLOT void onXmppConnected();
    Q_SLOT void onMucMessage(const QString &user, const QString &text);
    // telegram
    Q_SLOT void onTgMessage(qint64 tgChatId, int uid, const QString &text);
    Q_SLOT void onTgMemberAdded(qint64 tgChatId, const ChatUser &user);
    Q_SLOT void onTgMemberRemoved(qint64 tgChatId, int uid);
    Q_SLOT void onTgMemberPhotoChanged(qint64 tgChatId, int uid, const QByteArray &data);

    QString mapTgChatIdToMucJid(qint64 tgChatId);

    std::unique_ptr<BridgeControllerPrivate> d;
    std::unique_ptr<Database> m_database;
    TgBotClient *m_tgClient;
    XmppService *m_xmppService;
};
