// SPDX-FileCopyrightText: 2021 Linus Jahn <lnj@kaidan.im>
//
// SPDX-License-Identifier: AGPL-3.0-only

#include "database.h"

#include <QDebug>
#include <QDir>
#include <QSqlDatabase>
#include <QSqlDriver>
#include <QSqlError>
#include <QSqlField>
#include <QSqlQuery>
#include <QSqlRecord>
#include <QStandardPaths>
#include <QThreadPool>
#include <QThreadStorage>
#include <QRandomGenerator>
#include <QtConcurrent/QtConcurrentRun>

QSqlField createSqlField(const QString &key, const QVariant &val)
{
    QSqlField field(key, val.type());
    field.setValue(val);
    return field;
}

void prepareQuery(QSqlQuery &query, const QString &sql)
{
    if (!query.prepare(sql)) {
        qDebug() << "Failed to prepare query:" << sql;
        qFatal("QSqlError: %s", qPrintable(query.lastError().text()));
    }
}

void execQuery(QSqlQuery &query)
{
    if (!query.exec()) {
        qDebug() << "Failed to execute query:" << query.executedQuery();
        qFatal("QSqlError: %s", qPrintable(query.lastError().text()));
    }
}

void execQuery(QSqlQuery &query, const QString &sql)
{
    prepareQuery(query, sql);
    execQuery(query);
}

void execQuery(QSqlQuery &query,
               const QString &sql,
               const QVector<QVariant> &bindValues)
{
    prepareQuery(query, sql);

    for (const auto &val : bindValues) {
        query.addBindValue(val);
    }

    execQuery(query);
}

void execQuery(QSqlQuery &query,
                      const QString &sql,
                      const QMap<QString, QVariant> &bindValues)
{
    prepareQuery(query, sql);

    const QStringList bindKeys = bindValues.keys();
    for (const auto &key : bindKeys)
        query.bindValue(key, bindValues.value(key));

    execQuery(query);
}

struct Connection
{
    Q_DISABLE_COPY(Connection);

    QString name;
    QSqlDatabase database;

    Connection()
        : name(QString::number(QRandomGenerator::global()->generate(), 36)),
          database(QSqlDatabase::addDatabase(QStringLiteral("QSQLITE"), name))
    {
        if (!database.isValid()) {
            qFatal("Cannot add database: %s", qPrintable(database.lastError().text()));
        }

        const auto writeDir = QDir(QStandardPaths::writableLocation(QStandardPaths::AppDataLocation));
        if (!writeDir.mkpath(QLatin1String("."))) {
            qFatal("Failed to create writable directory at %s", qPrintable(writeDir.absolutePath()));
        }

        // Ensure that we have a writable location on all devices.
        const auto fileName = writeDir.absoluteFilePath(QStringLiteral("data.sqlite"));
        // open() will create the SQLite database if it doesn't exist.
        database.setDatabaseName(fileName);
        if (!database.open()) {
            qFatal("Cannot open database: %s", qPrintable(database.lastError().text()));
        }
        qDebug() << "[Database] Connection" << name << "opened.";
    }

    ~Connection()
    {
        qDebug() << "[Database] Connection" << name << "closed.";
        database = {};
        QSqlDatabase::removeDatabase(name);
    }
};

static QThreadStorage<Connection *> dbConnections;

class DatabasePrivate
{
public:
    QThreadPool pool;
    QMutex tableCreationMutex;
    bool tablesCreated = false;
};

Database::Database()
    : d(new DatabasePrivate)
{
    // 2 min expiry timeout
    d->pool.setExpiryTimeout(120000);
    d->pool.setMaxThreadCount(4);
}

Database::~Database()
{
}

QFuture<QVector<DbGroup>> Database::groups()
{
    return QtConcurrent::run(&d->pool, [=]() -> QVector<DbGroup> {
        thread_local static auto query = [this]() {
            auto query = createQuery();
            prepareQuery(query, "SELECT * FROM groups");
            return query;
        }();
        execQuery(query);

        auto rec = query.record();
        thread_local static const int protocolIndex = rec.indexOf("protocol");
        thread_local static const int mucJidIndex = rec.indexOf("muc_jid");
        thread_local static const int tgChatIdIndex = rec.indexOf("tg_chat_id");

        QVector<DbGroup> groups;
        while (query.next()) {
            DbGroup group;
            group.protocol = DbGroup::Protocol(query.value(protocolIndex).toInt());
            group.mucJid = query.value(mucJidIndex).toString();
            group.tgChatId = query.value(tgChatIdIndex).toLongLong();
            groups << group;
        }
        query.finish();
        return groups;
    });
}

QFuture<void> Database::insertGroup(const DbGroup &group)
{
    return QtConcurrent::run(&d->pool, [=]() {
        thread_local static auto query = [this]() {
            auto query = createQuery();
            prepareQuery(query, "INSERT OR REPLACE INTO groups VALUES (:1, :2, :3)");
            return query;
        }();
        query.addBindValue(int(group.protocol));
        query.addBindValue(group.mucJid);
        query.addBindValue(group.tgChatId);
        execQuery(query);
    });
}

QFuture<QVector<DbTgUser>> Database::tgUsers(qint64 chatId)
{
    return QtConcurrent::run(&d->pool, [=]() {
        thread_local static auto query = [this]() {
            auto query = createQuery();
            prepareQuery(
                query,
                "SELECT tg_user_id, username FROM tg_users WHERE tg_chat_id = :1"
            );
            return query;
        }();

        query.addBindValue(chatId);
        execQuery(query);

        auto rec = query.record();
        thread_local static const int indexUserId = rec.indexOf("tg_user_id");
        thread_local static const int indexUsername = rec.indexOf("username");

        QVector<DbTgUser> users;
        while (query.next()) {
            DbTgUser user;
            user.tgChatId = chatId;
            user.tgUserId = query.value(indexUserId).toLongLong();
            user.username = query.value(indexUsername).toString();
            users << user;
        }
        query.finish();
        return users;
    });
}

QFuture<void> Database::insertTgUser(const DbTgUser &user)
{
    return QtConcurrent::run(&d->pool, [=]() {
        thread_local static auto query = [this]() {
            auto query = createQuery();
            prepareQuery(query, "INSERT OR REPLACE INTO tg_users VALUES (:1, :2, :3)");
            return query;
        }();
        query.addBindValue(user.tgChatId);
        query.addBindValue(user.tgUserId);
        query.addBindValue(user.username);
        execQuery(query);
    });
}

QFuture<void> Database::removeTgUser(qint64 chatId, qint64 userId)
{
    return QtConcurrent::run(&d->pool, [=]() {
        thread_local static auto query = [this]() {
            auto query = createQuery();
            prepareQuery(query, "DELETE FROM tg_users WHERE tg_chat_id = :1 AND tg_user_id = :2");
            return query;
        }();
        query.addBindValue(chatId);
        query.addBindValue(userId);
        execQuery(query);
    });
}

void Database::createTables()
{
    QMutexLocker locker(&d->tableCreationMutex);
    if (d->tablesCreated) {
        return;
    }

    QSqlQuery query(currentDatabase());
    execQuery(
        query,
        "CREATE TABLE IF NOT EXISTS groups ("
        "protocol INTEGER, "
        "muc_jid TEXT, "
        "tg_chat_id INTEGER)"
    );
    execQuery(
        query,
        "CREATE TABLE IF NOT EXISTS tg_users ("
        "tg_chat_id INTEGER, "
        "tg_user_id INTEGER, "
        "username TEXT, "
        "PRIMARY KEY(tg_user_id, tg_chat_id))"
    );

    d->tablesCreated = true;
}

QSqlDatabase Database::currentDatabase()
{
    if (!dbConnections.hasLocalData()) {
        dbConnections.setLocalData(new Connection());
    }
    return dbConnections.localData()->database;
}

QSqlQuery Database::createQuery()
{
    createTables();
    QSqlQuery query(currentDatabase());
    query.setForwardOnly(true);
    return query;
}
