# SPDX-FileCopyrightText: 2021 Linus Jahn <lnj@kaidan.im>
#
# SPDX-License-Identifier: CC0-1.0

add_executable(qxbridge
    QXmppComponent.cpp
    QXmppComponentExtension.cpp
    QXmppComponentConfig.cpp
    QXmppOutgoingComponent.cpp
    main.cpp
    bridgecontroller.cpp
    xmppservice.cpp
    tgbotclient.cpp
    mucservice.cpp
    database.cpp
    chatuser.cpp
    qtgbot/api.cpp
    qtgbot/user.cpp
    qtgbot/response.cpp
    qtgbot/message.cpp
    qtgbot/document.cpp
    qtgbot/update.cpp
    qtgbot/chatmemberupdated.cpp
    qtgbot/chat.cpp
    qtgbot/chatmember.cpp
    qtgbot/chatinvitelink.cpp
    qtgbot/chatid.cpp
    qtgbot/sendmessage.cpp
    qtgbot/photosize.cpp
    qtgbot/file.cpp
)

target_link_libraries(qxbridge
    Qt${QT_VERSION_MAJOR}::Core
    Qt${QT_VERSION_MAJOR}::Network
    Qt${QT_VERSION_MAJOR}::Xml
    Qt${QT_VERSION_MAJOR}::Sql
    Qt${QT_VERSION_MAJOR}::Concurrent
    QXmpp::QXmpp
)
