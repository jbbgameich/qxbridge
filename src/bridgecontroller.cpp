// SPDX-FileCopyrightText: 2021 Linus Jahn <lnj@kaidan.im>
//
// SPDX-License-Identifier: AGPL-3.0-only

#include "bridgecontroller.h"

#include <QFutureWatcher>

#include <QXmppMessage.h>

#include "qtgbot/api.h"
#include "qtgbot/sendmessage.h"
#include "qtgbot/message.h"

#include "tgbotclient.h"
#include "xmppservice.h"
#include "mucservice.h"
#include "database.h"
#include "global.h"
#include "chatuser.h"

class BridgeControllerPrivate
{
public:
    QVector<DbGroup> groups;
};

BridgeController::BridgeController(QObject *parent)
    : QObject(parent),
      d(new BridgeControllerPrivate),
      m_database(new Database),
      m_tgClient(new TgBotClient(m_database.get(), this)),
      m_xmppService(new XmppService(this))
{
    connect(m_tgClient, &TgBotClient::messageReceived, this, &BridgeController::onTgMessage);
    connect(m_tgClient, &TgBotClient::memberAdded, this, &BridgeController::onTgMemberAdded);
    connect(m_tgClient, &TgBotClient::memberRemoved, this, &BridgeController::onTgMemberRemoved);
    connect(m_tgClient, &TgBotClient::memberPhotoChanged, this, &BridgeController::onTgMemberPhotoChanged);
    connect(m_xmppService, &XmppService::connected, this, &BridgeController::onXmppConnected);
    connect(m_xmppService->mucService(), &MucService::messageReceived, this, &BridgeController::onMucMessage);

    await(m_database->groups(), this, [this](QVector<DbGroup> &&groups) {
        d->groups = groups;
        m_tgClient->start();
        m_xmppService->connectToServer();
    });
}

BridgeController::~BridgeController() = default;

void BridgeController::onXmppConnected()
{
    const auto tgMembers = m_tgClient->currentMembers();
    std::for_each(tgMembers.keyBegin(), tgMembers.keyEnd(), [&](qint64 tgChatId) {
        for (const auto &user : tgMembers.value(tgChatId)) {
            onTgMemberAdded(tgChatId, user);
        }
    });
}

void BridgeController::onMucMessage(const QString &user, const QString &text)
{
    auto *api = m_tgClient->api();
    const auto escaped = QStringLiteral("*%1*: %2").arg(QTgBot::SendMessage::escapeMarkdownV2(user), QTgBot::SendMessage::escapeMarkdownV2(text));
    auto future = api->sendMessage(QTgBot::SendMessage(-1001350000600, escaped, QTgBot::SendMessage::MarkdownV2));
    await(future, this, [](const QTgBot::MessageResult &result) {
        if (const auto message = std::get_if<QTgBot::Message>(&result)) {
            qDebug() << "> Message sent to telegram.";
        } else if (const auto error = std::get_if<QTgBot::Error>(&result)) {
            qDebug() << "| Telegram: sendMessage failed:" << error->description;
        }
    });
}

void BridgeController::onTgMessage(qint64 tgChatId, int uid, const QString &text)
{
    if (!m_xmppService->isConnected()) {
        return;
    }

    if (const auto mucJid = mapTgChatIdToMucJid(tgChatId); !mucJid.isEmpty()) {
        m_xmppService->mucService()->sendMessage(mucJid, uid, text);
    }
}

void BridgeController::onTgMemberAdded(qint64 tgChatId, const ChatUser &user)
{
    if (!m_xmppService->isConnected()) {
        return;
    }

    if (const auto mucJid = mapTgChatIdToMucJid(tgChatId); !mucJid.isEmpty()) {
        m_xmppService->mucService()->addMember(mucJid, user);
    }
}

void BridgeController::onTgMemberRemoved(qint64 tgChatId, int uid)
{
    if (!m_xmppService->isConnected()) {
        return;
    }

    if (const auto mucJid = mapTgChatIdToMucJid(tgChatId); !mucJid.isEmpty()) {
        m_xmppService->mucService()->removeMember(mucJid, uid);
    }
}

void BridgeController::onTgMemberPhotoChanged(qint64 tgChatId, int uid, const QByteArray &data)
{
    if (const auto mucJid = mapTgChatIdToMucJid(tgChatId); !mucJid.isEmpty()) {
        m_xmppService->mucService()->updateMemberPhoto(mucJid, uid, data);
    }
}

QString BridgeController::mapTgChatIdToMucJid(qint64 tgChatId)
{
    const auto itr = std::find_if(d->groups.cbegin(), d->groups.cend(), [&tgChatId](const DbGroup &group) {
        return group.tgChatId == tgChatId;
    });
    if (itr != d->groups.cend()) {
        return itr->mucJid;
    }
    return {};
}
