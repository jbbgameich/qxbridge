// SPDX-FileCopyrightText: 2021 Linus Jahn <lnj@kaidan.im>
//
// SPDX-License-Identifier: AGPL-3.0-only

#pragma once

#include <cinttypes>
#include <variant>
#include <QString>

struct ChatUser
{
    int uid;
    // unique identifier from protocol
    QString protocolId;
    // non-unique nickname of the user
    QString nickname;

    static int genUserId();
};
