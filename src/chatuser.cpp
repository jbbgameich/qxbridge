// SPDX-FileCopyrightText: 2021 Linus Jahn <lnj@kaidan.im>
//
// SPDX-License-Identifier: AGPL-3.0-only

#include "chatuser.h"

int ChatUser::genUserId()
{
    static int idCounter = 0;
    return ++idCounter;
}
