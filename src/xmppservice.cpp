// SPDX-FileCopyrightText: 2021 Linus Jahn <lnj@kaidan.im>
//
// SPDX-License-Identifier: AGPL-3.0-only

#include "xmppservice.h"

#include <QSettings>
#include <QDebug>

#include <QXmppMessage.h>

#include "QXmppComponentConfig.h"
#include "mucservice.h"

XmppService::XmppService(QObject *parent)
    : QXmppComponent(parent),
      m_mucService(new MucService())
{
    logger()->setLoggingType(QXmppLogger::StdoutLogging);
    logger()->setMessageTypes(QXmppLogger::InformationMessage | QXmppLogger::DebugMessage | QXmppLogger::WarningMessage);
    logger()->setMessageTypes(QXmppLogger::AnyMessage);

    addExtension(m_mucService);
}

void XmppService::connectToServer()
{
    QSettings settings;

    QXmppComponentConfig config;
    config.setComponentName(settings.value("xmpp/component-name").toString());
    config.setHost(settings.value("xmpp/host").toString());
    config.setPort(settings.value("xmpp/port").toUInt());
    config.setSecret(settings.value("xmpp/secret").toString());

    if (config.componentName().isEmpty() ||
            config.host().isEmpty() ||
            !config.port() ||
            config.secret().isEmpty()) {
        qFatal("Invalid configuration: Invalid component name, host, port or secret!");
    }

    QXmppComponent::connectToServer(config);
}

MucService *XmppService::mucService() const
{
    return m_mucService;
}
