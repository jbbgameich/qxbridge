// SPDX-FileCopyrightText: 2021 Linus Jahn <lnj@kaidan.im>
//
// SPDX-License-Identifier: AGPL-3.0-only

#pragma once

#include <memory>
#include <QString>
#include <QFuture>

class QSqlDatabase;
class QSqlQuery;
class DatabasePrivate;

struct DbGroup
{
    enum Protocol : quint8 {
        TelegramMuc,
    };

    Protocol protocol;
    QString mucJid;
    qint64 tgChatId;
};

struct DbTgUser
{
    qint64 tgChatId = 0;
    qint64 tgUserId = 0;
    QString username;
};

class Database
{
public:
    Database();
    ~Database();

    // groups
    auto groups() -> QFuture<QVector<DbGroup>>;
    auto insertGroup(const DbGroup &group) -> QFuture<void>;

    // tg_users
    auto tgUsers(qint64 chatId) -> QFuture<QVector<DbTgUser>>;
    auto insertTgUser(const DbTgUser &user) -> QFuture<void>;
    auto removeTgUser(qint64 chatId, qint64 userId) -> QFuture<void>;

private:
    void createTables();
    QSqlDatabase currentDatabase();
    QSqlQuery createQuery();

    std::unique_ptr<DatabasePrivate> d;
};
