// SPDX-FileCopyrightText: 2021 Linus Jahn <lnj@kaidan.im>
//
// SPDX-License-Identifier: LGPL-2.0-only OR LGPL-3.0-only

#include "chatinvitelink.h"
#include "user.h"

namespace QTgBot {

class ChatInviteLinkPrivate : public QSharedData
{
public:
    QString inviteLink;
    User creator;
    bool isPrimary = false;
    bool isRevoked = false;
    qint64 expireDate = 0;
    int memberLimit = 0;
};

ChatInviteLink ChatInviteLink::fromJson(const QJsonObject &obj)
{
    ChatInviteLink link;
    link.setInviteLink(obj.value(u"invite_link").toString());
    link.setCreator(User::fromJson(obj.value(u"creator")));
    link.setIsPrimary(obj.value(u"is_primary").toBool());
    link.setIsRevoked(obj.value(u"is_revoked").toBool());
    link.setExpireDate(obj.value(u"expire_date").toDouble());
    link.setMemberLimit(obj.value(u"member_limit").toDouble());
    return link;
}

ChatInviteLink::ChatInviteLink()
    : d(new ChatInviteLinkPrivate)
{
}

ChatInviteLink::ChatInviteLink(const ChatInviteLink &) = default;
ChatInviteLink::~ChatInviteLink() = default;
ChatInviteLink &ChatInviteLink::operator=(const ChatInviteLink &) = default;

QString ChatInviteLink::inviteLink() const
{
    return d->inviteLink;
}

void ChatInviteLink::setInviteLink(const QString &inviteLink)
{
    d->inviteLink = inviteLink;
}

User ChatInviteLink::creator() const
{
    return d->creator;
}

void ChatInviteLink::setCreator(const User &creator)
{
    d->creator = creator;
}

bool ChatInviteLink::isPrimary() const
{
    return d->isPrimary;
}

void ChatInviteLink::setIsPrimary(bool isPrimary)
{
    d->isPrimary = isPrimary;
}

bool ChatInviteLink::isRevoked() const
{
    return d->isRevoked;
}

void ChatInviteLink::setIsRevoked(bool isRevoked)
{
    d->isRevoked = isRevoked;
}

qint64 ChatInviteLink::expireDate() const
{
    return d->expireDate;
}

void ChatInviteLink::setExpireDate(const qint64 &expireDate)
{
    d->expireDate = expireDate;
}

int ChatInviteLink::memberLimit() const
{
    return d->memberLimit;
}

void ChatInviteLink::setMemberLimit(int memberLimit)
{
    d->memberLimit = memberLimit;
}

}
