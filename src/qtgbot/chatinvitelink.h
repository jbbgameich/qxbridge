// SPDX-FileCopyrightText: 2021 Linus Jahn <lnj@kaidan.im>
//
// SPDX-License-Identifier: LGPL-2.0-only OR LGPL-3.0-only

#pragma once

#include <QJsonObject>
#include <QJsonArray>
#include <QSharedDataPointer>

#include "qtgbotglobal.h"

namespace QTgBot {

class ChatInviteLinkPrivate;
class User;

class ChatInviteLink
{
public:
    static ChatInviteLink fromJson(const QJsonObject &obj);
    FROM_JSON_OVERLOADS(ChatInviteLink)

    ChatInviteLink();
    ChatInviteLink(const ChatInviteLink &);
    ~ChatInviteLink();
    ChatInviteLink &operator=(const ChatInviteLink &);

    QString inviteLink() const;
    void setInviteLink(const QString &inviteLink);

    User creator() const;
    void setCreator(const User &creator);

    bool isPrimary() const;
    void setIsPrimary(bool isPrimary);

    bool isRevoked() const;
    void setIsRevoked(bool isRevoked);

    qint64 expireDate() const;
    void setExpireDate(const qint64 &expireDate);

    int memberLimit() const;
    void setMemberLimit(int memberLimit);

private:
    QSharedDataPointer<ChatInviteLinkPrivate> d;
};

}
