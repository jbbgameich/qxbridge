// SPDX-FileCopyrightText: 2021 Linus Jahn <lnj@kaidan.im>
//
// SPDX-License-Identifier: LGPL-2.0-only OR LGPL-3.0-only

#include "sendmessage.h"

#include <QJsonObject>

#include "chatid.h"

namespace QTgBot {

class SendMessagePrivate : public QSharedData
{
public:
    ChatId chatId = 0;
    QString text;
    SendMessage::ParseMode parseMode = SendMessage::None;
    // entities
    bool disableWebPagePreview = false;
    bool disableNotification = false;
    qint64 replyToMessageId = 0;
    bool allowSendingWithoutReply = false;
    // reply markup
};

QStringView parseModeToString(SendMessage::ParseMode mode)
{
    switch (mode) {
    case SendMessage::None:
        return {};
    case SendMessage::Markdown:
        return u"Markdown";
    case SendMessage::MarkdownV2:
        return u"MarkdownV2";
    case SendMessage::HTML:
        return u"HTML";
    }
    return {};
}

QString SendMessage::escapeMarkdownV2(const QString &str)
{
    static constexpr QStringView escapeCharacters = u"_*[]()~`>#+-=|{}.!";

    QString out;
    QTextStream stream(&out);
    for (const auto &character : str) {
        if (escapeCharacters.contains(character)) {
            stream << QChar(u'\\') << character;
        } else {
            stream << character;
        }
    }
    stream.flush();
    return out;
}

SendMessage::SendMessage()
    : d(new SendMessagePrivate)
{
}

SendMessage::SendMessage(const ChatId &chat, const QString &text, SendMessage::ParseMode parseMode, qint64 replyToMessageId)
    : d(new SendMessagePrivate)
{
    d->chatId = chat;
    d->text = text;
    d->parseMode = parseMode;
    d->replyToMessageId = replyToMessageId;
}

QJsonObject SendMessage::toJson() const
{
    QJsonObject obj;
    obj.insert(u"chat_id", d->chatId);
    obj.insert(u"text", d->text);
    if (auto mode = parseModeToString(d->parseMode); !mode.empty()) {
        obj.insert(u"parse_mode", mode.toString());
    }
    if (d->disableWebPagePreview) {
        obj.insert(u"disable_web_page_preview", true);
    }
    if (d->disableNotification) {
        obj.insert(u"disable_notification", true);
    }
    if (d->replyToMessageId) {
        obj.insert(u"reply_to_message_id", true);
    }
    if (d->allowSendingWithoutReply) {
        obj.insert(u"allow_sending_without_reply", true);
    }
    return obj;
}

SendMessage::SendMessage(const SendMessage &) = default;
SendMessage::~SendMessage() = default;
SendMessage &SendMessage::operator=(const SendMessage &) = default;

ChatId SendMessage::chatId() const
{
    return d->chatId;
}

void SendMessage::setChatId(const ChatId &chatId)
{
    d->chatId = chatId;
}

QString SendMessage::text() const
{
    return d->text;
}

void SendMessage::setText(const QString &text)
{
    d->text = text;
}

SendMessage::ParseMode SendMessage::parseMode() const
{
    return d->parseMode;
}

void SendMessage::setParseMode(const ParseMode &parseMode)
{
    d->parseMode = parseMode;
}

bool SendMessage::disableWebPagePreview() const
{
    return d->disableWebPagePreview;
}

void SendMessage::setDisableWebPagePreview(bool disableWebPagePreview)
{
    d->disableWebPagePreview = disableWebPagePreview;
}

bool SendMessage::disableNotification() const
{
    return d->disableNotification;
}

void SendMessage::setDisableNotification(bool disableNotification)
{
    d->disableNotification = disableNotification;
}

qint64 SendMessage::replyToMessageId() const
{
    return d->replyToMessageId;
}

void SendMessage::setReplyToMessageId(qint64 replyToMessageId)
{
    d->replyToMessageId = replyToMessageId;
}

bool SendMessage::allowSendingWithoutReply() const
{
    return d->allowSendingWithoutReply;
}

void SendMessage::setAllowSendingWithoutReply(bool allowSendingWithoutReply)
{
    d->allowSendingWithoutReply = allowSendingWithoutReply;
}

}
