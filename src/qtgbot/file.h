// SPDX-FileCopyrightText: 2021 Linus Jahn <lnj@kaidan.im>
//
// SPDX-License-Identifier: LGPL-2.0-only OR LGPL-3.0-only

#pragma once

#include <QSharedDataPointer>
#include <QJsonArray>
#include <QJsonObject>
#include <QJsonValue>
#include "qtgbotglobal.h"

namespace QTgBot {

class FilePrivate;

class File
{
public:
    static File fromJson(const QJsonObject &obj);
    FROM_JSON_OVERLOADS(File)

    File();
    File(const File &);
    ~File();
    File &operator=(const File &);

    QString fileId() const;
    void setFileId(const QString &fileId);

    QString fileUniqueId() const;
    void setFileUniqueId(const QString &fileUniqueId);

    int fileSize() const;
    void setFileSize(int fileSize);

    QString filePath() const;
    void setFilePath(const QString &filePath);

private:
    QSharedDataPointer<FilePrivate> d;
};

}
