#include "document.h"

namespace QTgBot {

class DocumentPrivate : public QSharedData
{
public:
    QString fileId;
    QString fileUniqueId;
    QString fileName;
    QString mimeType;
    int fileSize;
};

Document Document::fromJson(const QJsonObject &obj)
{
    Document doc;
    doc.setFileId(obj.value(u"file_id").toString());
    doc.setFileUniqueId(obj.value(u"file_unique_id").toString());
    doc.setFileName(obj.value(u"file_name").toString());
    doc.setMimeType(obj.value(u"mime_type").toString());
    doc.setFileSize(obj.value(u"file_size").toInt());
    return doc;
}

Document::Document()
    : d(new DocumentPrivate)
{
}

Document::Document(const Document &) = default;
Document::~Document() = default;
Document &Document::operator=(const Document &) = default;

QString Document::fileId() const
{
    return d->fileId;
}

void Document::setFileId(const QString &fileId)
{
    d->fileId = fileId;
}

QString Document::fileUniqueId() const
{
    return d->fileUniqueId;
}

void Document::setFileUniqueId(const QString &fileUniqueId)
{
    d->fileUniqueId = fileUniqueId;
}

QString Document::fileName() const
{
    return d->fileName;
}

void Document::setFileName(const QString &fileName)
{
    d->fileName = fileName;
}

QString Document::mimeType() const
{
    return d->mimeType;
}

void Document::setMimeType(const QString &mimeType)
{
    d->mimeType = mimeType;
}

int Document::fileSize() const
{
    return d->fileSize;
}

void Document::setFileSize(int fileSize)
{
    d->fileSize = fileSize;
}

}
