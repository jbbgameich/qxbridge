// SPDX-FileCopyrightText: 2021 Linus Jahn <lnj@kaidan.im>
//
// SPDX-License-Identifier: LGPL-2.0-only OR LGPL-3.0-only

#include "update.h"
#include "chatmemberupdated.h"

#include <QJsonArray>
#include <QJsonObject>
#include <QJsonValue>

#include "message.h"

namespace QTgBot {

class UpdatePrivate : public QSharedData
{
public:
    qint64 updateId = 0;
    std::optional<Message> message;
    std::optional<Message> editedMessage;
    std::optional<Message> channelPost;
    std::optional<Message> editedChannelPost;
    // TODO:
    // inline_query
    // chosen_inline_result
    // callback_query
    // shipping_query
    // pre_checkout_query
    // poll
    // poll_answer
    std::optional<ChatMemberUpdated> myChatMember;
    std::optional<ChatMemberUpdated> chatMember;
};

Update Update::fromJson(const QJsonObject &obj)
{
    Update update;
    update.setUpdateId(obj.value(u"update_id").toDouble());
    if (obj.contains(u"message")) {
        update.setMessage(Message::fromJson(obj.value(u"message")));
    }
    if (obj.contains(u"edited_message")) {
        update.setEditedMessage(Message::fromJson(obj.value(u"edited_message")));
    }
    if (obj.contains(u"channel_post")) {
        update.setChannelPost(Message::fromJson(obj.value(u"channel_post")));
    }
    if (obj.contains(u"edited_channel_post")) {
        update.setEditedChannelPost(Message::fromJson(obj.value(u"edited_channel_post")));
    }
    if (obj.contains(u"my_chat_member")) {

    }
    if (obj.contains(u"chat_member")) {

    }
    return update;
}

Update::Update()
    : d(new UpdatePrivate)
{
}

Update::Update(const Update &) = default;
Update::~Update() = default;
Update &Update::operator=(const Update &) = default;

qint64 Update::updateId() const
{
    return d->updateId;
}

void Update::setUpdateId(const qint64 &updateId)
{
    d->updateId = updateId;
}

std::optional<Message> Update::message() const
{
    return d->message;
}

void Update::setMessage(const std::optional<Message> &message)
{
    d->message = message;
}

std::optional<Message> Update::editedMessage() const
{
    return d->editedMessage;
}

void Update::setEditedMessage(const std::optional<Message> &editedMessage)
{
    d->editedMessage = editedMessage;
}

std::optional<Message> Update::channelPost() const
{
    return d->channelPost;
}

void Update::setChannelPost(const std::optional<Message> &channelPost)
{
    d->channelPost = channelPost;
}

std::optional<Message> Update::editedChannelPost() const
{
    return d->editedChannelPost;
}

void Update::setEditedChannelPost(const std::optional<Message> &editedChannelPost)
{
    d->editedChannelPost = editedChannelPost;
}

std::optional<ChatMemberUpdated> Update::myChatMember() const
{
    return d->myChatMember;
}

void Update::setMyChatMember(const std::optional<ChatMemberUpdated> &myChatMember)
{
    d->myChatMember = myChatMember;
}

std::optional<ChatMemberUpdated> Update::chatMember() const
{
    return d->chatMember;
}

void Update::setChatMember(const std::optional<ChatMemberUpdated> &chatMember)
{
    d->chatMember = chatMember;
}

}
