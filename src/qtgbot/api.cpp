// SPDX-FileCopyrightText: 2021 Linus Jahn <lnj@kaidan.im>
//
// SPDX-License-Identifier: LGPL-2.0-only OR LGPL-3.0-only

#include "api.h"

#include <QStringBuilder>
#include <QJsonDocument>
#include <QJsonObject>
#include <QFile>

#include "message.h"
#include "update.h"
#include "chatmember.h"
#include "photosize.h"
#include "file.h"

namespace QTgBot {

Result handleTrue(const QJsonValue &)
{
    return True();
}

IntResult handleInt(const QJsonValue &value)
{
    return value.toInt();
}

Api::Api(QNetworkAccessManager *netManager, QObject *parent)
    : QObject(parent),
      m_netManager(netManager)
{
}

QString Api::apiToken() const
{
    return m_apiToken;
}

void Api::setApiToken(const QString &apiToken)
{
    m_apiToken = apiToken;
}

QFuture<UserResult> Api::getMe()
{
    return get<UserResult>(u"getMe", User::fromJsonValue);
}

QFuture<UpdatesResult> Api::getUpdates(int offset, quint8 limit)
{
    QVariantMap params = {
        {
            "allowed_updates",
            QVariantList {
                "chat_member",
                "message",
                "edited_message",
                "channel_post",
                "edited_channel_post",
                "poll",
                "poll_answer",
                "my_chat_member",
                "chat_member"
            }
        },
    };

    if (offset) {
        params.insert("offset", offset);
    }
    if (limit) {
        if (limit > 100) {
            qDebug() << "[QTgBotApi] getUpdates: error: limit MUST be between 1 and 100.";
        }
        params.insert("limit", limit);
    }

    return post<UpdatesResult>(u"getUpdates", std::move(params), Update::fromJsonArrayValue);
}

QFuture<ChatResult> Api::getChat(const ChatId &chatId)
{
    return post<ChatResult>(u"getChat", {{"chat_id", QVariant(chatId)}}, Chat::fromJsonValue);
}

QFuture<ChatMemberResult> Api::getChatMember(const ChatId &chatId, qint64 userId)
{
    return post<ChatMemberResult>(u"getChatMember",
                                  {{"chat_id", QVariant(chatId)}, {"user_id", userId}},
                                  ChatMember::fromJsonValue);
}

QFuture<IntResult> Api::getChatMembersCount(const ChatId &chatId)
{
    return post<IntResult>(u"getChatMembersCount", {{"chat_id", QVariant(chatId)}}, handleInt);
}

QFuture<UserProfilePhotosResult> Api::getUserProfilePhotos(qint64 userId, int limit, int offset)
{
    QVariantMap params = {
        {"user_id", userId},
    };
    if (limit) {
        params.insert("limit", limit);
    }
    if (offset) {
        params.insert("offset", offset);
    }
    return post<UserProfilePhotosResult>(u"getUserProfilePhotos", std::move(params), UserProfilePhotos::fromJsonValue);
}

QFuture<FileResult> Api::getFile(const QString &fileId)
{
    return post<FileResult>(u"getFile", QVariantMap {{"file_id", fileId}}, File::fromJsonValue);
}

QFuture<Result> Api::deleteChatPhoto(const ChatId &chatId)
{
    return post<Result>(u"deleteChatPhoto", {{"chat_id", QVariant(chatId)}}, handleTrue);
}

QFuture<Result> Api::setChatTitle(const ChatId &chatId, const QString &title)
{
    return post<Result>(u"setChatTitle", {{"chat_id", QVariant(chatId)}, {"title", title}}, handleTrue);
}

QFuture<Result> Api::setChatDescription(const ChatId &chatId, const QString &description)
{
    return post<Result>(u"setChatDescription", {{"chat_id", QVariant(chatId)}, {"description", description}}, handleTrue);
}

QFuture<Result> Api::leaveChat(const ChatId &chatId)
{
    return post<Result>(u"leaveChat", {{"chat_id", QVariant(chatId)}}, handleTrue);
}

QFuture<MessageResult> Api::sendMessage(const SendMessage &message)
{
    return post<MessageResult>(u"sendMessage", message.toJson(), Message::fromJsonValue);
}

QFuture<DownloadFileResult> Api::downloadToFile(const QString &url, std::unique_ptr<QFile> output)
{
    auto interface = std::make_shared<QFutureInterface<DownloadFileResult>>(QFutureInterfaceBase::Started);
    std::shared_ptr<QFile> file(std::move(output));

    if (!file->isOpen()) {
        if (!file->open(QFile::WriteOnly)) {
            interface->reportResult(std::pair(file->error(), file->errorString()));
            interface->reportFinished();
        }
    } else if (file->openMode() != QFile::WriteOnly) {
        interface->reportResult(std::pair(QFileDevice::OpenError, QStringLiteral("File opened with wrong openMode.")));
        interface->reportFinished();
    }

    QNetworkRequest request(url);
    QNetworkReply *reply = m_netManager->get(request);

    connect(reply, &QNetworkReply::finished, this, [=]() {
        interface->reportResult(True());
        interface->reportFinished();
        reply->deleteLater();
        file->close();
    });
#if QT_VERSION >= QT_VERSION_CHECK(5, 15, 0)
    connect(reply, &QNetworkReply::errorOccurred, this, [=](QNetworkReply::NetworkError error) {
#else
    connect(reply, QOverload<QNetworkReply::NetworkError>::of(&QNetworkReply::error), this, [=](QNetworkReply::NetworkError error) {
#endif
        interface->reportResult(Error { error, reply->errorString() });
        interface->reportFinished();
        reply->deleteLater();
        file->close();
    });
    connect(reply, &QNetworkReply::readyRead, this, [=]() {
        file->write(reply->readAll());
    });
    return interface->future();
}

QFuture<DownloadResult> Api::download(const QString &url)
{
    return reportResults<DownloadResult>(m_netManager->get(QNetworkRequest(QUrl(url))), [](QNetworkReply *reply) {
        return reply->readAll();
    });
}

QFuture<DownloadResult> Api::downloadFilePath(const QString &filePath)
{
    return download(u"https://api.telegram.org/file/bot" % m_apiToken % u'/' % filePath);
}

QString Api::endpoint(QStringView method)
{
    return u"https://api.telegram.org/bot" % m_apiToken % u'/' % method;
}

}
