// SPDX-FileCopyrightText: 2021 Linus Jahn <lnj@kaidan.im>
//
// SPDX-License-Identifier: LGPL-2.0-only OR LGPL-3.0-only

#pragma once

#include <variant>

#include <QObject>
#include <QFuture>
#include <QFileDevice>
#include <QNetworkReply>
#include <QJsonDocument>

#include "qtgbotglobal.h"
#include "chat.h"
#include "chatid.h"
#include "user.h"
#include "response.h"
#include "sendmessage.h"

class QNetworkAccessManager;
class QFile;

namespace QTgBot {

class Update;
class ChatMember;
class UserProfilePhotos;
class File;

struct True {};
struct Error {
    QNetworkReply::NetworkError error;
    QString description;
};

using Updates = QVector<Update>;

using Result = std::variant<True, Error>;
using IntResult = std::variant<int, Error>;
using UpdatesResult = std::variant<Updates, Error>;
using UserResult = std::variant<User, Error>;
using UserProfilePhotosResult = std::variant<UserProfilePhotos, Error>;
using ChatResult = std::variant<Chat, Error>;
using ChatMemberResult = std::variant<ChatMember, Error>;
using MessageResult = std::variant<Message, Error>;
using FileResult = std::variant<File, Error>;

using FileError = std::pair<QFileDevice::FileError, QString>;
using DownloadFileResult = std::variant<True, Error, FileError>;
using DownloadResult = std::variant<QByteArray, Error>;

class Api : public QObject
{
    Q_OBJECT

public:
    explicit Api(QNetworkAccessManager *netManager, QObject *parent = nullptr);

    QString apiToken() const;
    void setApiToken(const QString &apiToken);

    auto getMe() -> QFuture<UserResult>;
    auto getUpdates(int offset = 0, quint8 limit = 0) -> QFuture<UpdatesResult>;
    auto getChat(const ChatId &chatId) -> QFuture<ChatResult>;
    auto getChatMember(const ChatId &chatId, qint64 userId) -> QFuture<ChatMemberResult>;
    auto getChatMembersCount(const ChatId &chatId) -> QFuture<IntResult>;
    auto getUserProfilePhotos(qint64 userId, int limit = 1, int offset = 0) -> QFuture<UserProfilePhotosResult>;
    auto getFile(const QString &fileId) -> QFuture<FileResult>;
    auto deleteChatPhoto(const ChatId &chatId) -> QFuture<Result>;
    auto setChatTitle(const ChatId &chatId, const QString &title) -> QFuture<Result>;
    auto setChatDescription(const ChatId &chatId, const QString &description) -> QFuture<Result>;
    auto leaveChat(const ChatId &chatId) -> QFuture<Result>;
    auto sendMessage(const SendMessage &message) -> QFuture<MessageResult>;
    auto downloadToFile(const QString &url, std::unique_ptr<QFile> output) -> QFuture<DownloadFileResult>;
    auto download(const QString &url) -> QFuture<DownloadResult>;
    auto downloadFilePath(const QString &filePath) -> QFuture<DownloadResult>;

private:
    QString endpoint(QStringView endpoint);

    template<typename T, typename Processor>
    QFuture<T> reportResults(QNetworkReply *reply, Processor processs)
    {
        auto interface = std::make_shared<QFutureInterface<T>>(QFutureInterfaceBase::Started);

        connect(reply, &QNetworkReply::finished, this, [=] () {
            interface->reportResult(processs(reply));
            interface->reportFinished();
            reply->deleteLater();
        });
        connect(reply, &QNetworkReply::errorOccurred, this, [=](QNetworkReply::NetworkError error) {
            interface->reportResult(Error { error, reply->errorString() });
            interface->reportFinished();
            reply->deleteLater();
        });

        return interface->future();
    }

    template<typename T, typename Processor>
    QFuture<T> processResponse(QNetworkReply *reply, Processor process)
    {
        return reportResults<T>(reply, [process { std::move(process) }](QNetworkReply *reply) -> T {
            auto doc = QJsonDocument::fromJson(reply->readAll());
            auto resp = Response::fromJson(doc.object());
            if (resp.ok()) {
                return process(resp.result());
            } else {
                return Error {
                    QNetworkReply::UnknownContentError,
                    resp.description()
                };
            }
        });
    }

    template<typename T, typename Processor>
    QFuture<T> get(QNetworkRequest &&request, Processor processs)
    {
        return processResponse<T>(m_netManager->get(request), processs);
    }
    template<typename T, typename Processor>
    QFuture<T> get(QStringView function, Processor processs)
    {
        return processResponse<T>(m_netManager->get(QNetworkRequest(endpoint(function))), processs);
    }

    template<typename T, typename Processor>
    QFuture<T> post(QNetworkRequest &&request, const QByteArray &data, Processor processs)
    {
//        qDebug() << data;
        return processResponse<T>(m_netManager->post(request, data), processs);
    }
    template<typename T, typename Processor>
    QFuture<T> post(QStringView function, QJsonObject &&data, Processor processs)
    {
        const auto jsonData = QJsonDocument(data).toJson(QJsonDocument::Compact);
        auto request = QNetworkRequest(endpoint(function));
        request.setHeader(QNetworkRequest::ContentTypeHeader, "application/json");
        return post<T>(std::move(request), jsonData, std::move(processs));
    }
    template<typename T, typename Processor>
    QFuture<T> post(QStringView function, QVariantMap &&data, Processor processs)
    {
        return post<T>(function, QJsonObject::fromVariantMap(data), std::move(processs));
    }

    QNetworkAccessManager *m_netManager;
    QString m_apiToken;
};

}
