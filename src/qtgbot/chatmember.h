// SPDX-FileCopyrightText: 2021 Linus Jahn <lnj@kaidan.im>
//
// SPDX-License-Identifier: LGPL-2.0-only OR LGPL-3.0-only

#pragma once

#include <QJsonObject>
#include <QJsonArray>
#include <QSharedDataPointer>

#include "qtgbotglobal.h"

namespace QTgBot {

class ChatMemberPrivate;
class User;

class ChatMember
{
public:
    static ChatMember fromJson(const QJsonObject &);
    FROM_JSON_OVERLOADS(ChatMember)

    ChatMember();
    ChatMember(const ChatMember &);
    ~ChatMember();
    ChatMember &operator=(const ChatMember &);

    User user() const;
    void setUser(const User &user);

    QString status() const;
    void setStatus(const QString &status);

    QString customTitle() const;
    void setCustomTitle(const QString &customTitle);

    bool isAnonymous() const;
    void setIsAnonymous(bool isAnonymous);

    bool canBeEdited() const;
    void setCanBeEdited(bool canBeEdited);

    bool canManageChat() const;
    void setCanManageChat(bool canManageChat);

    bool canPostMessages() const;
    void setCanPostMessages(bool canPostMessages);

    bool canEditMessages() const;
    void setCanEditMessages(bool canEditMessages);

    bool canDeleteMessages() const;
    void setCanDeleteMessages(bool canDeleteMessages);

    bool canManageVoiceChats() const;
    void setCanManageVoiceChats(bool canManageVoiceChats);

    bool canRestrictMembers() const;
    void setCanRestrictMembers(bool canRestrictMembers);

    bool canPromoteMembers() const;
    void setCanPromoteMembers(bool canPromoteMembers);

    bool canChangeInfo() const;
    void setCanChangeInfo(bool canChangeInfo);

    bool canInviteUsers() const;
    void setCanInviteUsers(bool canInviteUsers);

    bool canPinMessages() const;
    void setCanPinMessages(bool canPinMessages);

    bool isMember() const;
    void setIsMember(bool isMember);

    bool canSendMessages() const;
    void setCanSendMessages(bool canSendMessages);

    bool canSendMediaMessages() const;
    void setCanSendMediaMessages(bool canSendMediaMessages);

    bool canSendPolls() const;
    void setCanSendPolls(bool canSendPolls);

    bool canSendOtherMessages() const;
    void setCanSendOtherMessages(bool canSendOtherMessages);

    bool canAddWebPagePreviews() const;
    void setCanAddWebPagePreviews(bool canAddWebPagePreviews);

    qint64 untilDate() const;
    void setUntilDate(qint64 untilDate);

private:
    QSharedDataPointer<ChatMemberPrivate> d;
};

}
