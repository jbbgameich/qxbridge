// SPDX-FileCopyrightText: 2021 Linus Jahn <lnj@kaidan.im>
//
// SPDX-License-Identifier: LGPL-2.0-only OR LGPL-3.0-only

#include "chat.h"
#include "message.h"

namespace QTgBot {

class ChatPrivate : public QSharedData
{
public:
    qint64 id = 0;
    Chat::Type type = Chat::None;
    QString title;
    QString username;
    QString firstName;
    QString lastName;
    // photo
    QString bio;
    QString description;
    QString inviteLink;
    std::optional<Message> pinnedMessage;
    // chat permissions
    qint64 slowModeDelay = 0;
    qint64 messageAutoDeleteTime = 0;
    QString stickerSetName;
    std::optional<bool> canSetStickerSet = false;
    qint64 linkedChatId = 0;
    // location
};

Chat::Type typeFromString(const QString &str)
{
    if (str == u"private") {
        return Chat::Private;
    }
    if (str == u"group") {
        return Chat::Group;
    }
    if (str == u"supergroup") {
        return Chat::Supergroup;
    }
    if (str == u"channel") {
        return Chat::Channel;
    }
    return Chat::None;
}

Chat Chat::fromJson(const QJsonObject &obj)
{
    Chat chat;
    chat.setId(obj.value(u"id").toDouble());
    chat.setType(typeFromString(obj.value(u"type").toString()));
    chat.setTitle(obj.value(u"title").toString());
    chat.setUsername(obj.value(u"username").toString());
    chat.setFirstName(obj.value(u"first_name").toString());
    chat.setLastName(obj.value(u"last_name").toString());
    chat.setBio(obj.value(u"bio").toString());
    chat.setDescription(obj.value(u"description").toString());
    chat.setInviteLink(obj.value(u"invite_link").toString());
    if (obj.contains(u"pinned_message")) {
        chat.setPinnedMessage(Message::fromJson(obj.value(u"pinned_message")));
    }
    chat.setSlowModeDelay(obj.value(u"slow_mode_delay").toDouble());
    chat.setMessageAutoDeleteTime(obj.value(u"message_auto_delete_time").toDouble());
    chat.setStickerSetName(obj.value(u"sticker_set_name").toString());
    if (obj.contains(u"can_set_sticker_set")) {
        chat.setCanSetStickerSet(obj.value(u"can_set_sticker_set").toBool());
    }
    chat.setLinkedChatId(obj.value(u"linked_chat_id").toDouble());
    return chat;
}

Chat::Chat()
    : d(new ChatPrivate)
{
}


Chat::Chat(const Chat &) = default;
Chat::~Chat() = default;
Chat &Chat::operator=(const Chat &) = default;

qint64 Chat::id() const
{
    return d->id;
}

void Chat::setId(qint64 id)
{
    d->id = id;
}

Chat::Type Chat::type() const
{
    return d->type;
}

void Chat::setType(Type type)
{
    d->type = type;
}

QString Chat::title() const
{
    return d->title;
}

void Chat::setTitle(const QString &title)
{
    d->title = title;
}

QString Chat::username() const
{
    return d->username;
}

void Chat::setUsername(const QString &username)
{
    d->username = username;
}

QString Chat::firstName() const
{
    return d->firstName;
}

void Chat::setFirstName(const QString &firstName)
{
    d->firstName = firstName;
}

QString Chat::lastName() const
{
    return d->lastName;
}

void Chat::setLastName(const QString &lastName)
{
    d->lastName = lastName;
}

QString Chat::bio() const
{
    return d->bio;
}

void Chat::setBio(const QString &bio)
{
    d->bio = bio;
}

QString Chat::description() const
{
    return d->description;
}

void Chat::setDescription(const QString &description)
{
    d->description = description;
}

QString Chat::inviteLink() const
{
    return d->inviteLink;
}

void Chat::setInviteLink(const QString &inviteLink)
{
    d->inviteLink = inviteLink;
}

std::optional<Message> Chat::pinnedMessage() const
{
    return d->pinnedMessage;
}

void Chat::setPinnedMessage(const std::optional<Message> &pinnedMessage)
{
    d->pinnedMessage = pinnedMessage;
}

qint64 Chat::slowModeDelay() const
{
    return d->slowModeDelay;
}

void Chat::setSlowModeDelay(qint64 slowModeDelay)
{
    d->slowModeDelay = slowModeDelay;
}

qint64 Chat::messageAutoDeleteTime() const
{
    return d->messageAutoDeleteTime;
}

void Chat::setMessageAutoDeleteTime(qint64 messageAutoDeleteTime)
{
    d->messageAutoDeleteTime = messageAutoDeleteTime;
}

QString Chat::stickerSetName() const
{
    return d->stickerSetName;
}

void Chat::setStickerSetName(const QString &stickerSetName)
{
    d->stickerSetName = stickerSetName;
}

std::optional<bool> Chat::canSetStickerSet() const
{
    return d->canSetStickerSet;
}

void Chat::setCanSetStickerSet(const std::optional<bool> &canSetStickerSet)
{
    d->canSetStickerSet = canSetStickerSet;
}

qint64 Chat::linkedChatId() const
{
    return d->linkedChatId;
}

void Chat::setLinkedChatId(const qint64 &linkedChatId)
{
    d->linkedChatId = linkedChatId;
}

}
