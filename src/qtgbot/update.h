// SPDX-FileCopyrightText: 2021 Linus Jahn <lnj@kaidan.im>
//
// SPDX-License-Identifier: LGPL-2.0-only OR LGPL-3.0-only

#pragma once

#include <optional>

#include <QSharedDataPointer>
#include <QJsonObject>
#include <QJsonArray>

#include "qtgbotglobal.h"

class QJsonObject;
class QJsonArray;

namespace QTgBot {

class UpdatePrivate;
class Message;
class ChatMemberUpdated;

class Update
{
public:
    static Update fromJson(const QJsonObject &);
    FROM_JSON_OVERLOADS(Update)

    Update();
    Update(const Update &);
    ~Update();
    Update &operator=(const Update &);

    qint64 updateId() const;
    void setUpdateId(const qint64 &updateId);

    std::optional<Message> message() const;
    void setMessage(const std::optional<Message> &message);

    std::optional<Message> editedMessage() const;
    void setEditedMessage(const std::optional<Message> &editedMessage);

    std::optional<Message> channelPost() const;
    void setChannelPost(const std::optional<Message> &channelPost);

    std::optional<Message> editedChannelPost() const;
    void setEditedChannelPost(const std::optional<Message> &editedChannelPost);

    std::optional<ChatMemberUpdated> myChatMember() const;
    void setMyChatMember(const std::optional<ChatMemberUpdated> &myChatMember);

    std::optional<ChatMemberUpdated> chatMember() const;
    void setChatMember(const std::optional<ChatMemberUpdated> &chatMember);

private:
    QSharedDataPointer<UpdatePrivate> d;
};

}
