// SPDX-FileCopyrightText: 2021 Linus Jahn <lnj@kaidan.im>
//
// SPDX-License-Identifier: LGPL-2.0-only OR LGPL-3.0-only

#pragma once

#include <optional>

#include <QString>
#include <QJsonObject>
#include <QJsonArray>
#include <QSharedDataPointer>

#include "qtgbotglobal.h"

namespace QTgBot {

class ChatPrivate;
class Message;

class Chat
{
public:
    static Chat fromJson(const QJsonObject &);
    FROM_JSON_OVERLOADS(Chat)

    enum Type {
        None,
        Private,
        Group,
        Supergroup,
        Channel,
    };

    Chat();
    Chat(const Chat &);
    ~Chat();
    Chat &operator=(const Chat &);

    qint64 id() const;
    void setId(qint64 id);

    Type type() const;
    void setType(Type type);

    QString title() const;
    void setTitle(const QString &title);

    QString username() const;
    void setUsername(const QString &username);

    QString firstName() const;
    void setFirstName(const QString &firstName);

    QString lastName() const;
    void setLastName(const QString &lastName);

    QString bio() const;
    void setBio(const QString &bio);

    QString description() const;
    void setDescription(const QString &description);

    QString inviteLink() const;
    void setInviteLink(const QString &inviteLink);

    std::optional<Message> pinnedMessage() const;
    void setPinnedMessage(const std::optional<Message> &pinnedMessage);

    qint64 slowModeDelay() const;
    void setSlowModeDelay(qint64 slowModeDelay);

    qint64 messageAutoDeleteTime() const;
    void setMessageAutoDeleteTime(qint64 messageAutoDeleteTime);

    QString stickerSetName() const;
    void setStickerSetName(const QString &stickerSetName);

    std::optional<bool> canSetStickerSet() const;
    void setCanSetStickerSet(const std::optional<bool> &canSetStickerSet);

    qint64 linkedChatId() const;
    void setLinkedChatId(const qint64 &linkedChatId);

private:
    QSharedDataPointer<ChatPrivate> d;
};

}
