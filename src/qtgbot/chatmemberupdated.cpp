// SPDX-FileCopyrightText: 2021 Linus Jahn <lnj@kaidan.im>
//
// SPDX-License-Identifier: LGPL-2.0-only OR LGPL-3.0-only

#include "chatmemberupdated.h"

#include "chat.h"
#include "user.h"
#include "chatmember.h"
#include "chatinvitelink.h"

namespace QTgBot {

class ChatMemberUpdatedPrivate : public QSharedData
{
public:
    Chat chat;
    User from;
    qint64 date;
    ChatMember oldChatMember;
    ChatMember newChatMember;
    ChatInviteLink inviteLink;
};

ChatMemberUpdated ChatMemberUpdated::fromJson(const QJsonObject &obj)
{
    ChatMemberUpdated update;
    update.setChat(Chat::fromJson(obj.value(u"chat")));
    update.setFrom(User::fromJson(obj.value(u"from")));
    update.setDate(obj.value(u"date").toDouble());
    update.setOldChatMember(ChatMember::fromJson(obj.value(u"old_chat_member")));
    update.setNewChatMember(ChatMember::fromJson(obj.value(u"new_chat_member")));
    update.setInviteLink(ChatInviteLink::fromJson(obj.value(u"invite_link")));
    return update;
}

ChatMemberUpdated::ChatMemberUpdated()
    : d(new ChatMemberUpdatedPrivate)
{
}

ChatMemberUpdated::ChatMemberUpdated(const ChatMemberUpdated &) = default;
ChatMemberUpdated::~ChatMemberUpdated() = default;
ChatMemberUpdated &ChatMemberUpdated::operator=(const ChatMemberUpdated &) = default;

Chat ChatMemberUpdated::chat() const
{
    return d->chat;
}

void ChatMemberUpdated::setChat(const Chat &chat)
{
    d->chat = chat;
}

User ChatMemberUpdated::from() const
{
    return d->from;
}

void ChatMemberUpdated::setFrom(const User &from)
{
    d->from = from;
}

qint64 ChatMemberUpdated::date() const
{
    return d->date;
}

void ChatMemberUpdated::setDate(qint64 date)
{
    d->date = date;
}

ChatMember ChatMemberUpdated::oldChatMember() const
{
    return d->oldChatMember;
}

void ChatMemberUpdated::setOldChatMember(const ChatMember &oldChatMember)
{
    d->oldChatMember = oldChatMember;
}

ChatMember ChatMemberUpdated::newChatMember() const
{
    return d->newChatMember;
}

void ChatMemberUpdated::setNewChatMember(const ChatMember &newChatMember)
{
    d->newChatMember = newChatMember;
}

ChatInviteLink ChatMemberUpdated::inviteLink() const
{
    return d->inviteLink;
}

void ChatMemberUpdated::setInviteLink(const ChatInviteLink &inviteLink)
{
    d->inviteLink = inviteLink;
}

}
