// SPDX-FileCopyrightText: 2021 Linus Jahn <lnj@kaidan.im>
//
// SPDX-License-Identifier: LGPL-2.0-only OR LGPL-3.0-only

#pragma once

#define FROM_JSON_OVERLOADS(className) \
    static className fromJson(const QJsonValue &val) \
    { \
        return fromJson(val.toObject()); \
    } \
    static className fromJsonValue(const QJsonValue &val) \
    { \
        return fromJson(val); \
    } \
    static QVector<className> fromJson(const QJsonArray &array) \
    { \
        QVector<className> items(array.size()); \
        for (const auto &val : array) { \
            items << fromJson(val); \
        } \
        return items; \
    } \
    static QVector<className> fromJsonArray(const QJsonArray &array) \
    { \
        return fromJson(array); \
    } \
    static QVector<className> fromJsonArrayValue(const QJsonValue &value) \
    { \
        return fromJson(value.toArray()); \
    }

