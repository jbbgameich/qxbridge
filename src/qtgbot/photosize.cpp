// SPDX-FileCopyrightText: 2021 Linus Jahn <lnj@kaidan.im>
//
// SPDX-License-Identifier: LGPL-2.0-only OR LGPL-3.0-only

#include "photosize.h"

namespace QTgBot {

class PhotoSizePrivate : public QSharedData
{
public:
    QString fileId;
    QString fileUniqueId;
    int width = 0;
    int height = 0;
    int fileSize = 0;
};

PhotoSize PhotoSize::fromJson(const QJsonObject &obj)
{
    PhotoSize size;
    size.setFileId(obj.value(u"file_id").toString());
    size.setFileUniqueId(obj.value(u"file_unique_id").toString());
    size.setWidth(obj.value(u"width").toInt());
    size.setHeight(obj.value(u"height").toInt());
    size.setFileSize(obj.value(u"file_size").toInt());
    return size;
}

PhotoSize::PhotoSize()
    : d(new PhotoSizePrivate)
{
}

PhotoSize::PhotoSize(const PhotoSize &) = default;
PhotoSize::~PhotoSize() = default;
PhotoSize &PhotoSize::operator=(const PhotoSize &) = default;

QString PhotoSize::fileId() const
{
    return d->fileId;
}

void PhotoSize::setFileId(const QString &fileId)
{
    d->fileId = fileId;
}

QString PhotoSize::fileUniqueId() const
{
    return d->fileUniqueId;
}

void PhotoSize::setFileUniqueId(const QString &fileUniqueId)
{
    d->fileUniqueId = fileUniqueId;
}

int PhotoSize::width() const
{
    return d->width;
}

void PhotoSize::setWidth(int width)
{
    d->width = width;
}

int PhotoSize::height() const
{
    return d->height;
}

void PhotoSize::setHeight(int height)
{
    d->height = height;
}

int PhotoSize::fileSize() const
{
    return d->fileSize;
}

void PhotoSize::setFileSize(int fileSize)
{
    d->fileSize = fileSize;
}

class UserProfilePhotosPrivate : public QSharedData
{
public:
    int totalCount = 0;
    QVector<QVector<PhotoSize>> photos;
};

UserProfilePhotos UserProfilePhotos::fromJson(const QJsonObject &obj)
{
    UserProfilePhotos photos;
    photos.setTotalCount(obj.value(u"total_count").toInt());
    const auto photosList = obj.value(u"photos").toArray();
    for (const auto &value : photosList) {
        photos.d->photos << PhotoSize::fromJsonArray(value.toArray());
    }
    return photos;
}

UserProfilePhotos::UserProfilePhotos()
    : d(new UserProfilePhotosPrivate)
{
}

UserProfilePhotos::UserProfilePhotos(const UserProfilePhotos &) = default;
UserProfilePhotos::~UserProfilePhotos() = default;
UserProfilePhotos &UserProfilePhotos::operator=(const UserProfilePhotos &) = default;

int UserProfilePhotos::totalCount() const
{
    return d->totalCount;
}

void UserProfilePhotos::setTotalCount(int totalCount)
{
    d->totalCount = totalCount;
}

QVector<QVector<PhotoSize>> UserProfilePhotos::photos() const
{
    return d->photos;
}

void UserProfilePhotos::setPhotos(const QVector<QVector<PhotoSize>> &photos)
{
    d->photos = photos;
}

PhotoSize UserProfilePhotos::pick(int size) const
{
    if (d->photos.isEmpty()) {
        return {};
    }
    const auto photoSizes = d->photos.first();
    for (const auto &photoSize : photoSizes) {
        if (photoSize.width() >= size) {
            return photoSize;
        }
    }
    if (!photoSizes.isEmpty()) {
        return photoSizes.first();
    }
    return {};
}

}
