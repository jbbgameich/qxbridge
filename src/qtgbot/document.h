#pragma once

#include <QSharedDataPointer>
#include <QJsonObject>
#include <QJsonArray>
#include <QJsonValue>

#include "qtgbotglobal.h"

namespace QTgBot {

class DocumentPrivate;

class Document
{
public:
    static Document fromJson(const QJsonObject &obj);
    FROM_JSON_OVERLOADS(Document)

    Document();
    Document(const Document &);
    ~Document();
    Document &operator=(const Document &);

    QString fileId() const;
    void setFileId(const QString &fileId);

    QString fileUniqueId() const;
    void setFileUniqueId(const QString &fileUniqueId);

    QString fileName() const;
    void setFileName(const QString &fileName);

    QString mimeType() const;
    void setMimeType(const QString &mimeType);

    int fileSize() const;
    void setFileSize(int fileSize);

private:
    QSharedDataPointer<DocumentPrivate> d;
};

}
