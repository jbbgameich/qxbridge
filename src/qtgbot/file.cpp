// SPDX-FileCopyrightText: 2021 Linus Jahn <lnj@kaidan.im>
//
// SPDX-License-Identifier: LGPL-2.0-only OR LGPL-3.0-only

#include "file.h"

namespace QTgBot {

class FilePrivate : public QSharedData
{
public:
    QString fileId;
    QString fileUniqueId;
    int fileSize;
    QString filePath;
};

File File::fromJson(const QJsonObject &obj)
{
    File file;
    file.setFileId(obj.value(u"file_id").toString());
    file.setFileUniqueId(obj.value(u"file_unique_id").toString());
    file.setFileSize(obj.value(u"file_size").toInt());
    file.setFilePath(obj.value(u"file_path").toString());
    return file;
}

File::File()
    : d(new FilePrivate)
{
}

File::File(const File &) = default;
File::~File() = default;
File &File::operator=(const File &) = default;

QString File::fileId() const
{
    return d->fileId;
}

void File::setFileId(const QString &fileId)
{
    d->fileId = fileId;
}

QString File::fileUniqueId() const
{
    return d->fileUniqueId;
}

void File::setFileUniqueId(const QString &fileUniqueId)
{
    d->fileUniqueId = fileUniqueId;
}

int File::fileSize() const
{
    return d->fileSize;
}

void File::setFileSize(int fileSize)
{
    d->fileSize = fileSize;
}

QString File::filePath() const
{
    return d->filePath;
}

void File::setFilePath(const QString &filePath)
{
    d->filePath = filePath;
}

}
