// SPDX-FileCopyrightText: 2021 Linus Jahn <lnj@kaidan.im>
//
// SPDX-License-Identifier: LGPL-2.0-only OR LGPL-3.0-only

#include "user.h"

namespace QTgBot {

User User::fromJson(const QJsonObject &obj)
{
    User user;
    user.setId(obj.value(u"id").toDouble());
    user.setFirstName(obj.value(u"first_name").toString());
    user.setLastName(obj.value(u"last_name").toString());
    user.setUsername(obj.value(u"username").toString());
    user.setLanguageCode(obj.value(u"language_code").toString());
    user.setCanJoinGroups(obj.value(u"can_join_groups").toBool());
    user.setCanReadAllGroupMessages(obj.value(u"can_read_all_group_messages").toBool());
    user.setSupportsInlineQueries(obj.value(u"supports_inline_queries").toBool());
    return user;
}

User::User()
{

}

qint64 User::id() const
{
    return m_id;
}

void User::setId(const qint64 &id)
{
    m_id = id;
}

QString User::firstName() const
{
    return m_firstName;
}

void User::setFirstName(const QString &firstName)
{
    m_firstName = firstName;
}

QString User::lastName() const
{
    return m_lastName;
}

void User::setLastName(const QString &lastName)
{
    m_lastName = lastName;
}

QString User::username() const
{
    return m_username;
}

void User::setUsername(const QString &username)
{
    m_username = username;
}

QString User::languageCode() const
{
    return m_languageCode;
}

void User::setLanguageCode(const QString &languageCode)
{
    m_languageCode = languageCode;
}

bool User::isBot() const
{
    return m_isBot;
}

void User::setIsBot(bool isBot)
{
    m_isBot = isBot;
}

bool User::canJoinGroups() const
{
    return m_canJoinGroups;
}

void User::setCanJoinGroups(bool canJoinGroups)
{
    m_canJoinGroups = canJoinGroups;
}

bool User::canReadAllGroupMessages() const
{
    return m_canReadAllGroupMessages;
}

void User::setCanReadAllGroupMessages(bool canReadAllGroupMessages)
{
    m_canReadAllGroupMessages = canReadAllGroupMessages;
}

bool User::supportsInlineQueries() const
{
    return m_supportsInlineQueries;
}

void User::setSupportsInlineQueries(bool supportsInlineQueries)
{
    m_supportsInlineQueries = supportsInlineQueries;
}

}
