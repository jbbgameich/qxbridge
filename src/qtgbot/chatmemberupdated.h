// SPDX-FileCopyrightText: 2021 Linus Jahn <lnj@kaidan.im>
//
// SPDX-License-Identifier: LGPL-2.0-only OR LGPL-3.0-only

#pragma once

#include <QJsonObject>
#include <QJsonArray>
#include <QSharedDataPointer>

#include "qtgbotglobal.h"

namespace QTgBot {

class ChatMemberUpdatedPrivate;
class Chat;
class ChatMember;
class ChatInviteLink;
class User;

class ChatMemberUpdated
{
public:
    static ChatMemberUpdated fromJson(const QJsonObject &);
    FROM_JSON_OVERLOADS(ChatMemberUpdated)

    ChatMemberUpdated();
    ChatMemberUpdated(const ChatMemberUpdated &);
    ~ChatMemberUpdated();
    ChatMemberUpdated &operator=(const ChatMemberUpdated &);

    Chat chat() const;
    void setChat(const Chat &chat);

    User from() const;
    void setFrom(const User &from);

    qint64 date() const;
    void setDate(qint64 date);

    ChatMember oldChatMember() const;
    void setOldChatMember(const ChatMember &oldChatMember);

    ChatMember newChatMember() const;
    void setNewChatMember(const ChatMember &newChatMember);

    ChatInviteLink inviteLink() const;
    void setInviteLink(const ChatInviteLink &inviteLink);

private:
    QSharedDataPointer<ChatMemberUpdatedPrivate> d;
};

}
