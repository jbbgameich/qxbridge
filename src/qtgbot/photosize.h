// SPDX-FileCopyrightText: 2021 Linus Jahn <lnj@kaidan.im>
//
// SPDX-License-Identifier: LGPL-2.0-only OR LGPL-3.0-only

#pragma once

#include <QSharedDataPointer>
#include <QJsonArray>
#include <QJsonObject>
#include <QJsonValue>

#include "qtgbotglobal.h"


namespace QTgBot {

class PhotoSizePrivate;
class UserProfilePhotosPrivate;

class PhotoSize
{
public:
    static PhotoSize fromJson(const QJsonObject &);
    FROM_JSON_OVERLOADS(PhotoSize)

    PhotoSize();
    PhotoSize(const PhotoSize &);
    ~PhotoSize();
    PhotoSize &operator=(const PhotoSize &);

    QString fileId() const;
    void setFileId(const QString &fileId);

    QString fileUniqueId() const;
    void setFileUniqueId(const QString &fileUniqueId);

    int width() const;
    void setWidth(int width);

    int height() const;
    void setHeight(int height);

    int fileSize() const;
    void setFileSize(int fileSize);

private:
    QSharedDataPointer<PhotoSizePrivate> d;
};

class UserProfilePhotos
{
public:
    static UserProfilePhotos fromJson(const QJsonObject &);
    FROM_JSON_OVERLOADS(UserProfilePhotos)

    UserProfilePhotos();
    UserProfilePhotos(const UserProfilePhotos &);
    ~UserProfilePhotos();
    UserProfilePhotos &operator=(const UserProfilePhotos &);

    int totalCount() const;
    void setTotalCount(int totalCount);

    QVector<QVector<PhotoSize>> photos() const;
    void setPhotos(const QVector<QVector<PhotoSize>> &photos);

    PhotoSize pick(int size) const;

private:
    QSharedDataPointer<UserProfilePhotosPrivate> d;
};

}
