// SPDX-FileCopyrightText: 2021 Linus Jahn <lnj@kaidan.im>
//
// SPDX-License-Identifier: LGPL-2.0-only OR LGPL-3.0-only

#include <QVector>
#include <QSharedDataPointer>

#include "user.h"

namespace QTgBot {

struct MessageEntity {};

class MessagePrivate;
class Chat;
class Document;

class Message
{
public:
    static Message fromJson(const QJsonObject &);
    FROM_JSON_OVERLOADS(Message)

    Message();
    Message(const Message &);
    ~Message();
    Message &operator=(const Message &);

    qint64 messageId() const;
    void setMessageId(qint64 messageId);

    std::optional<User> from() const;
    void setFrom(const std::optional<User> &from);

    std::optional<Chat> senderChat() const;
    void setSenderChat(const std::optional<Chat> &senderChat);

    qint64 date() const;
    void setDate(qint64 date);

    Chat chat() const;
    void setChat(const Chat &chat);

    std::optional<User> forwardFrom() const;
    void setForwardFrom(const std::optional<User> &forwardFrom);

    std::optional<Chat> forwardFromChat() const;
    void setForwardFromChat(const std::optional<Chat> &forwardFromChat);

    qint64 forwardFromMessageId() const;
    void setForwardFromMessageId(qint64 forwardFromMessageId);

    QString forwardSignature() const;
    void setForwardSignature(const QString &forwardSignature);

    QString forwardSenderName() const;
    void setForwardSenderName(const QString &forwardSenderName);

    qint64 forwardDate() const;
    void setForwardDate(qint64 forwardDate);

    std::optional<Message> replyToMessage() const;
    void setReplyToMessage(const std::optional<Message> &replyToMessage);

    std::optional<User> viaBot() const;
    void setViaBot(const std::optional<User> &viaBot);

    qint64 editDate() const;
    void setEditDate(qint64 editDate);

    QString mediaGroupId() const;
    void setMediaGroupId(const QString &mediaGroupId);

    QString authorSignature() const;
    void setAuthorSignature(const QString &authorSignature);

    QString text() const;
    void setText(const QString &text);

    QVector<MessageEntity> entities() const;
    void setEntities(const QVector<MessageEntity> &entities);

    QString caption() const;
    void setCaption(const QString &caption);

    QVector<MessageEntity> captionEntities() const;
    void setCaptionEntities(const QVector<MessageEntity> &captionEntities);

    QVector<User> newChatMembers() const;
    void setNewChatMembers(const QVector<User> &newChatMembers);

    std::optional<User> leftChatMember() const;
    void setLeftChatMember(const std::optional<User> &leftChatMember);

    QString newChatTitle() const;
    void setNewChatTitle(const QString &newChatTitle);

    bool deleteChatPhoto() const;
    void setDeleteChatPhoto(bool deleteChatPhoto);

    bool groupChatCreated() const;
    void setGroupChatCreated(bool groupChatCreated);

    bool supergroupChatCreated() const;
    void setSupergroupChatCreated(bool supergroupChatCreated);

    bool channelChatCreated() const;
    void setChannelChatCreated(bool channelChatCreated);

    qint64 migrateToChatId() const;
    void setMigrateToChatId(qint64 migrateToChatId);

    qint64 migrateFromChatId() const;
    void setMigrateFromChatId(qint64 migrateFromChatId);

    std::optional<Message> pinnedMessage() const;
    void setPinnedMessage(const std::optional<Message> &pinnedMessage);

    std::optional<Document> animation() const;
    void setAnimation(const std::optional<Document> &animation);

    std::optional<Document> audio() const;
    void setAudio(const std::optional<Document> &audio);

    std::optional<Document> document() const;
    void setDocument(const std::optional<Document> &document);

    std::optional<Document> photo() const;
    void setPhoto(const std::optional<Document> &photo);

    std::optional<Document> sticker() const;
    void setSticker(const std::optional<Document> &sticker);

    std::optional<Document> video() const;
    void setVideo(const std::optional<Document> &video);

    std::optional<Document> videoNote() const;
    void setVideoNote(const std::optional<Document> &videoNote);

    std::optional<Document> voice() const;
    void setVoice(const std::optional<Document> &voice);

private:
    QSharedDataPointer<MessagePrivate> d;
};

}
