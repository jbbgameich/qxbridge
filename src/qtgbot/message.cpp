// SPDX-FileCopyrightText: 2021 Linus Jahn <lnj@kaidan.im>
//
// SPDX-License-Identifier: LGPL-2.0-only OR LGPL-3.0-only

#include "message.h"
#include "chat.h"
#include "document.h"

#include <QJsonObject>

namespace QTgBot {

template<typename T>
std::optional<T> parseOptional(const QJsonObject &obj, QStringView attribute)
{
    if (obj.contains(attribute)) {
        return T::fromJson(obj.value(attribute));
    }
    return std::nullopt;
}

class MessagePrivate : public QSharedData
{
public:
    qint64 messageId;
    std::optional<User> from;
    std::optional<Chat> senderChat;
    qint64 date = 0;
    Chat chat;
    std::optional<User> forwardFrom;
    std::optional<Chat> forwardFromChat;
    qint64 forwardFromMessageId = 0;
    QString forwardSignature;
    QString forwardSenderName;
    qint64 forwardDate = 0;
    std::optional<Message> replyToMessage;
    std::optional<User> viaBot;
    qint64 editDate = 0;
    QString mediaGroupId;
    QString authorSignature;
    QString text;
    QVector<MessageEntity> entities;
    std::optional<Document> animation;
    std::optional<Document> audio;
    std::optional<Document> document;
    std::optional<Document> photo;
    std::optional<Document> sticker;
    std::optional<Document> video;
    std::optional<Document> videoNote;
    std::optional<Document> voice;
    QString caption;
    QVector<MessageEntity> captionEntities;
    // contact
    // dice
    // game
    // poll
    // venue
    // location
    QVector<User> newChatMembers;
    std::optional<User> leftChatMember;
    QString newChatTitle;
    // new_chat_photo
    bool deleteChatPhoto;
    bool groupChatCreated;
    bool supergroupChatCreated;
    bool channelChatCreated;
    // message_auto_delete_timer_changed
    qint64 migrateToChatId = 0;
    qint64 migrateFromChatId = 0;
    std::optional<Message> pinnedMessage;
};

Message Message::fromJson(const QJsonObject &obj)
{
    Message msg;
    msg.setMessageId(obj.value(u"message_id").toDouble());
    msg.setFrom(User::fromJson(obj.value(u"from").toObject()));
    if (obj.contains(u"sender_chat")) {
        msg.setSenderChat(Chat::fromJson(obj.value(u"sender_chat")));
    }
    msg.setDate(obj.value(u"date").toDouble());
    msg.setChat(Chat::fromJson(obj.value(u"chat")));
    if (obj.contains(u"forward_from")) {
        msg.setForwardFrom(User::fromJson(obj.value(u"forward_from")));
    }
    if (obj.contains(u"forward_from_chat")) {
        msg.setForwardFromChat(Chat::fromJson(obj.value(u"forward_from_chat")));
    }
    msg.setForwardFromMessageId(obj.value(u"forward_from_message_id").toDouble());
    msg.setForwardSignature(obj.value(u"forward_signature").toString());
    msg.setForwardSenderName(obj.value(u"forward_sender_name").toString());
    msg.setForwardDate(obj.value(u"forward_date").toDouble());
    if (obj.contains(u"reply_to_message")) {
        msg.setReplyToMessage(Message::fromJson(obj.value(u"reply_to_message")));
    }
    if (obj.contains(u"via_bot")) {
        msg.setViaBot(User::fromJson(obj.value(u"via_bot")));
    }
    msg.setEditDate(obj.value(u"edit_date").toDouble());
    msg.setMediaGroupId(obj.value(u"media_group_id").toString());
    msg.setAuthorSignature(obj.value(u"author_signature").toString());
    msg.setText(obj.value(u"text").toString());
    // entities
    msg.setAnimation(parseOptional<Document>(obj, u"animation"));
    msg.setAudio(parseOptional<Document>(obj, u"audio"));
    msg.setDocument(parseOptional<Document>(obj, u"document"));
    msg.setPhoto(parseOptional<Document>(obj, u"photo"));
    msg.setSticker(parseOptional<Document>(obj, u"sticker"));
    msg.setVideo(parseOptional<Document>(obj, u"video"));
    msg.setVideoNote(parseOptional<Document>(obj, u"video_note"));
    msg.setVoice(parseOptional<Document>(obj, u"voice"));
    msg.setCaption(obj.value(u"caption").toString());
    // caption entities
    msg.setNewChatMembers(User::fromJson(obj.value(u"new_chat_members").toArray()));
    if (obj.contains(u"left_chat_member")) {
        msg.setLeftChatMember(User::fromJson(obj.value(u"left_chat_member")));
    }
    msg.setNewChatTitle(obj.value(u"new_chat_title").toString());
    msg.setDeleteChatPhoto(obj.contains(u"delete_chat_photo"));
    msg.setGroupChatCreated(obj.contains(u"group_chat_created"));
    msg.setSupergroupChatCreated(obj.contains(u"supergroup_chat_created"));
    msg.setChannelChatCreated(obj.contains(u"channel_chat_created"));
    msg.setMigrateToChatId(obj.value(u"migrate_to_chat_id").toDouble());
    msg.setMigrateFromChatId(obj.value(u"migrate_from_chat_id").toDouble());
    if (obj.contains(u"pinned_message")) {
        msg.setPinnedMessage(Message::fromJson(obj.value(u"pinned_message")));
    }
    return msg;
}

Message::Message()
    : d(new MessagePrivate)
{
}

Message::Message(const Message &) = default;
Message::~Message() = default;
Message &Message::operator=(const Message &) = default;

qint64 Message::messageId() const
{
    return d->messageId;
}

void Message::setMessageId(qint64 messageId)
{
    d->messageId = messageId;
}

std::optional<User> Message::from() const
{
    return d->from;
}

void Message::setFrom(const std::optional<User> &from)
{
    d->from = from;
}

std::optional<Chat> Message::senderChat() const
{
    return d->senderChat;
}

void Message::setSenderChat(const std::optional<Chat> &senderChat)
{
    d->senderChat = senderChat;
}

qint64 Message::date() const
{
    return d->date;
}

void Message::setDate(qint64 date)
{
    d->date = date;
}

Chat Message::chat() const
{
    return d->chat;
}

void Message::setChat(const Chat &chat)
{
    d->chat = chat;
}

std::optional<User> Message::forwardFrom() const
{
    return d->forwardFrom;
}

void Message::setForwardFrom(const std::optional<User> &forwardedFrom)
{
    d->forwardFrom = forwardedFrom;
}

std::optional<Chat> Message::forwardFromChat() const
{
    return d->forwardFromChat;
}

void Message::setForwardFromChat(const std::optional<Chat> &forwardedFromChat)
{
    d->forwardFromChat = forwardedFromChat;
}

qint64 Message::forwardFromMessageId() const
{
    return d->forwardFromMessageId;
}

void Message::setForwardFromMessageId(qint64 forwardedFromMessageId)
{
    d->forwardFromMessageId = forwardedFromMessageId;
}

QString Message::forwardSignature() const
{
    return d->forwardSignature;
}

void Message::setForwardSignature(const QString &forwardedSignature)
{
    d->forwardSignature = forwardedSignature;
}

QString Message::forwardSenderName() const
{
    return d->forwardSenderName;
}

void Message::setForwardSenderName(const QString &forwardedSenderName)
{
    d->forwardSenderName = forwardedSenderName;
}

qint64 Message::forwardDate() const
{
    return d->forwardDate;
}

void Message::setForwardDate(qint64 forwardedDate)
{
    d->forwardDate = forwardedDate;
}

std::optional<Message> Message::replyToMessage() const
{
    return d->replyToMessage;
}

void Message::setReplyToMessage(const std::optional<Message> &replyToMessage)
{
    d->replyToMessage = replyToMessage;
}

std::optional<User> Message::viaBot() const
{
    return d->viaBot;
}

void Message::setViaBot(const std::optional<User> &viaBot)
{
    d->viaBot = viaBot;
}

qint64 Message::editDate() const
{
    return d->editDate;
}

void Message::setEditDate(qint64 editDate)
{
    d->editDate = editDate;
}

QString Message::mediaGroupId() const
{
    return d->mediaGroupId;
}

void Message::setMediaGroupId(const QString &mediaGroupId)
{
    d->mediaGroupId = mediaGroupId;
}

QString Message::authorSignature() const
{
    return d->authorSignature;
}

void Message::setAuthorSignature(const QString &authorSignature)
{
    d->authorSignature = authorSignature;
}

QString Message::text() const
{
    return d->text;
}

void Message::setText(const QString &text)
{
    d->text = text;
}

QVector<MessageEntity> Message::entities() const
{
    return d->entities;
}

void Message::setEntities(const QVector<MessageEntity> &entities)
{
    d->entities = entities;
}

QString Message::caption() const
{
    return d->caption;
}

void Message::setCaption(const QString &caption)
{
    d->caption = caption;
}

QVector<MessageEntity> Message::captionEntities() const
{
    return d->captionEntities;
}

void Message::setCaptionEntities(const QVector<MessageEntity> &captionEntities)
{
    d->captionEntities = captionEntities;
}

QVector<User> Message::newChatMembers() const
{
    return d->newChatMembers;
}

void Message::setNewChatMembers(const QVector<User> &newChatMembers)
{
    d->newChatMembers = newChatMembers;
}

std::optional<User> Message::leftChatMember() const
{
    return d->leftChatMember;
}

void Message::setLeftChatMember(const std::optional<User> &leftChatMember)
{
    d->leftChatMember = leftChatMember;
}

QString Message::newChatTitle() const
{
    return d->newChatTitle;
}

void Message::setNewChatTitle(const QString &newChatTitle)
{
    d->newChatTitle = newChatTitle;
}

bool Message::deleteChatPhoto() const
{
    return d->deleteChatPhoto;
}

void Message::setDeleteChatPhoto(bool deleteChatPhoto)
{
    d->deleteChatPhoto = deleteChatPhoto;
}

bool Message::groupChatCreated() const
{
    return d->groupChatCreated;
}

void Message::setGroupChatCreated(bool groupChatCreated)
{
    d->groupChatCreated = groupChatCreated;
}

bool Message::supergroupChatCreated() const
{
    return d->supergroupChatCreated;
}

void Message::setSupergroupChatCreated(bool supergroupChatCreated)
{
    d->supergroupChatCreated = supergroupChatCreated;
}

bool Message::channelChatCreated() const
{
    return d->channelChatCreated;
}

void Message::setChannelChatCreated(bool channelChatCreated)
{
    d->channelChatCreated = channelChatCreated;
}

qint64 Message::migrateToChatId() const
{
    return d->migrateToChatId;
}

void Message::setMigrateToChatId(qint64 migrateToChatId)
{
    d->migrateToChatId = migrateToChatId;
}

qint64 Message::migrateFromChatId() const
{
    return d->migrateFromChatId;
}

void Message::setMigrateFromChatId(qint64 migrateFromChatId)
{
    d->migrateFromChatId = migrateFromChatId;
}

std::optional<Message> Message::pinnedMessage() const
{
    return d->pinnedMessage;
}

void Message::setPinnedMessage(const std::optional<Message> &pinnedMessage)
{
    d->pinnedMessage = pinnedMessage;
}

std::optional<Document> Message::animation() const
{
    return d->animation;
}

void Message::setAnimation(const std::optional<Document> &animation)
{
    d->animation = animation;
}

std::optional<Document> Message::audio() const
{
    return d->audio;
}

void Message::setAudio(const std::optional<Document> &audio)
{
    d->audio = audio;
}

std::optional<Document> Message::document() const
{
    return d->document;
}

void Message::setDocument(const std::optional<Document> &document)
{
    d->document = document;
}

std::optional<Document> Message::photo() const
{
    return d->photo;
}

void Message::setPhoto(const std::optional<Document> &photo)
{
    d->photo = photo;
}

std::optional<Document> Message::sticker() const
{
    return d->sticker;
}

void Message::setSticker(const std::optional<Document> &sticker)
{
    d->sticker = sticker;
}

std::optional<Document> Message::video() const
{
    return d->video;
}

void Message::setVideo(const std::optional<Document> &video)
{
    d->video = video;
}

std::optional<Document> Message::videoNote() const
{
    return d->videoNote;
}

void Message::setVideoNote(const std::optional<Document> &videoNote)
{
    d->videoNote = videoNote;
}

std::optional<Document> Message::voice() const
{
    return d->voice;
}

void Message::setVoice(const std::optional<Document> &voice)
{
    d->voice = voice;
}

}
