// SPDX-FileCopyrightText: 2021 Linus Jahn <lnj@kaidan.im>
//
// SPDX-License-Identifier: LGPL-2.0-only OR LGPL-3.0-only

#pragma once

#include <QSharedDataPointer>

#include "qtgbotglobal.h"

class QJsonObject;

namespace QTgBot {

class SendMessagePrivate;
class ChatId;

class SendMessage
{
public:
    static QString escapeMarkdownV2(const QString &str);

    enum ParseMode {
        None,
        Markdown,
        MarkdownV2,
        HTML,
    };

    SendMessage();
    SendMessage(const ChatId &chat, const QString &text, ParseMode = None, qint64 replyToMessageId = 0);
    SendMessage(const SendMessage &);
    ~SendMessage();
    SendMessage &operator=(const SendMessage &);

    QJsonObject toJson() const;

    ChatId chatId() const;
    void setChatId(const ChatId &chatId);

    QString text() const;
    void setText(const QString &text);

    ParseMode parseMode() const;
    void setParseMode(const ParseMode &parseMode);

    bool disableWebPagePreview() const;
    void setDisableWebPagePreview(bool disableWebPagePreview);

    bool disableNotification() const;
    void setDisableNotification(bool disableNotification);

    qint64 replyToMessageId() const;
    void setReplyToMessageId(qint64 replyToMessageId);

    bool allowSendingWithoutReply() const;
    void setAllowSendingWithoutReply(bool allowSendingWithoutReply);

private:
    QSharedDataPointer<SendMessagePrivate> d;
};

}
