// SPDX-FileCopyrightText: 2021 Linus Jahn <lnj@kaidan.im>
//
// SPDX-License-Identifier: LGPL-2.0-only OR LGPL-3.0-only

#include "chatmember.h"
#include "user.h"

namespace QTgBot {

class ChatMemberPrivate : public QSharedData
{
public:
    User user;
    QString status;
    QString customTitle;
    bool isAnonymous = false;
    bool canBeEdited = false;
    bool canManageChat = false;
    bool canPostMessages = false;
    bool canEditMessages = false;
    bool canDeleteMessages = false;
    bool canManageVoiceChats = false;
    bool canRestrictMembers = false;
    bool canPromoteMembers = false;
    bool canChangeInfo = false;
    bool canInviteUsers = false;
    bool canPinMessages = false;
    bool isMember = false;
    bool canSendMessages = false;
    bool canSendMediaMessages = false;
    bool canSendPolls = false;
    bool canSendOtherMessages = false;
    bool canAddWebPagePreviews = false;
    qint64 untilDate = 0;
};

ChatMember ChatMember::fromJson(const QJsonObject &obj)
{
    ChatMember member;
    member.setUser(User::fromJson(obj.value(u"user")));
    member.setStatus(obj.value(u"status").toString());
    member.setCustomTitle(obj.value(u"custom_title").toString());
    member.setIsAnonymous(obj.value(u"is_anonymous").toBool());
    member.setCanBeEdited(obj.value(u"can_be_edited").toBool());
    member.setCanManageChat(obj.value(u"can_manage_chat").toBool());
    member.setCanPostMessages(obj.value(u"can_post_messages").toBool());
    member.setCanEditMessages(obj.value(u"can_edit_messages").toBool());
    member.setCanDeleteMessages(obj.value(u"can_delete_messages").toBool());
    member.setCanManageVoiceChats(obj.value(u"can_manage_voice_chats").toBool());
    member.setCanRestrictMembers(obj.value(u"can_restrict_members").toBool());
    member.setCanPromoteMembers(obj.value(u"can_promote_members").toBool());
    member.setCanChangeInfo(obj.value(u"can_change_info").toBool());
    member.setCanInviteUsers(obj.value(u"can_invite_users").toBool());
    member.setCanPinMessages(obj.value(u"can_pin_messages").toBool());
    member.setIsMember(obj.value(u"is_member").toBool());
    member.setCanSendMessages(obj.value(u"can_send_messages").toBool());
    member.setCanSendMediaMessages(obj.value(u"can_send_media_messages").toBool());
    member.setCanSendPolls(obj.value(u"can_send_polls").toBool());
    member.setCanSendOtherMessages(obj.value(u"can_send_other_messages").toBool());
    member.setCanAddWebPagePreviews(obj.value(u"can_add_web_page_previews").toBool());
    member.setUntilDate(obj.value(u"until_date").toDouble());
    return member;
}

ChatMember::ChatMember()
    : d(new ChatMemberPrivate)
{
}

ChatMember::ChatMember(const ChatMember &) = default;
ChatMember::~ChatMember() = default;
ChatMember &ChatMember::operator=(const ChatMember &) = default;

User ChatMember::user() const
{
    return d->user;
}

void ChatMember::setUser(const User &user)
{
    d->user = user;
}

QString ChatMember::status() const
{
    return d->status;
}

void ChatMember::setStatus(const QString &status)
{
    d->status = status;
}

QString ChatMember::customTitle() const
{
    return d->customTitle;
}

void ChatMember::setCustomTitle(const QString &customTitle)
{
    d->customTitle = customTitle;
}

bool ChatMember::isAnonymous() const
{
    return d->isAnonymous;
}

void ChatMember::setIsAnonymous(bool isAnonymous)
{
    d->isAnonymous = isAnonymous;
}

bool ChatMember::canBeEdited() const
{
    return d->canBeEdited;
}

void ChatMember::setCanBeEdited(bool canBeEdited)
{
    d->canBeEdited = canBeEdited;
}

bool ChatMember::canManageChat() const
{
    return d->canManageChat;
}

void ChatMember::setCanManageChat(bool canManageChat)
{
    d->canManageChat = canManageChat;
}

bool ChatMember::canPostMessages() const
{
    return d->canPostMessages;
}

void ChatMember::setCanPostMessages(bool canPostMessages)
{
    d->canPostMessages = canPostMessages;
}

bool ChatMember::canEditMessages() const
{
    return d->canEditMessages;
}

void ChatMember::setCanEditMessages(bool canEditMessages)
{
    d->canEditMessages = canEditMessages;
}

bool ChatMember::canDeleteMessages() const
{
    return d->canDeleteMessages;
}

void ChatMember::setCanDeleteMessages(bool canDeleteMessages)
{
    d->canDeleteMessages = canDeleteMessages;
}

bool ChatMember::canManageVoiceChats() const
{
    return d->canManageVoiceChats;
}

void ChatMember::setCanManageVoiceChats(bool canManageVoiceChats)
{
    d->canManageVoiceChats = canManageVoiceChats;
}

bool ChatMember::canRestrictMembers() const
{
    return d->canRestrictMembers;
}

void ChatMember::setCanRestrictMembers(bool canRestrictMembers)
{
    d->canRestrictMembers = canRestrictMembers;
}

bool ChatMember::canPromoteMembers() const
{
    return d->canPromoteMembers;
}

void ChatMember::setCanPromoteMembers(bool canPromoteMembers)
{
    d->canPromoteMembers = canPromoteMembers;
}

bool ChatMember::canChangeInfo() const
{
    return d->canChangeInfo;
}

void ChatMember::setCanChangeInfo(bool canChangeInfo)
{
    d->canChangeInfo = canChangeInfo;
}

bool ChatMember::canInviteUsers() const
{
    return d->canInviteUsers;
}

void ChatMember::setCanInviteUsers(bool canInviteUsers)
{
    d->canInviteUsers = canInviteUsers;
}

bool ChatMember::canPinMessages() const
{
    return d->canPinMessages;
}

void ChatMember::setCanPinMessages(bool canPinMessages)
{
    d->canPinMessages = canPinMessages;
}

bool ChatMember::isMember() const
{
    return d->isMember;
}

void ChatMember::setIsMember(bool isMember)
{
    d->isMember = isMember;
}

bool ChatMember::canSendMessages() const
{
    return d->canSendMessages;
}

void ChatMember::setCanSendMessages(bool canSendMessages)
{
    d->canSendMessages = canSendMessages;
}

bool ChatMember::canSendMediaMessages() const
{
    return d->canSendMediaMessages;
}

void ChatMember::setCanSendMediaMessages(bool canSendMediaMessages)
{
    d->canSendMediaMessages = canSendMediaMessages;
}

bool ChatMember::canSendPolls() const
{
    return d->canSendPolls;
}

void ChatMember::setCanSendPolls(bool canSendPolls)
{
    d->canSendPolls = canSendPolls;
}

bool ChatMember::canSendOtherMessages() const
{
    return d->canSendOtherMessages;
}

void ChatMember::setCanSendOtherMessages(bool canSendOtherMessages)
{
    d->canSendOtherMessages = canSendOtherMessages;
}

bool ChatMember::canAddWebPagePreviews() const
{
    return d->canAddWebPagePreviews;
}

void ChatMember::setCanAddWebPagePreviews(bool canAddWebPagePreviews)
{
    d->canAddWebPagePreviews = canAddWebPagePreviews;
}

qint64 ChatMember::untilDate() const
{
    return d->untilDate;
}

void ChatMember::setUntilDate(qint64 unitDate)
{
    d->untilDate = unitDate;
}

}
