// SPDX-FileCopyrightText: 2021 Linus Jahn <lnj@kaidan.im>
//
// SPDX-License-Identifier: LGPL-2.0-only OR LGPL-3.0-only

#include "chatid.h"

#include <QVariant>
#include <QJsonValue>

namespace QTgBot {

ChatId::ChatId(qint64 id)
    : m_id(id)
{
}

ChatId::ChatId(const QString &username)
    : m_id(username)
{
}

QTgBot::ChatId::operator QVariant() const
{
    if (auto intId = std::get_if<qint64>(&m_id)) {
        return double(*intId);
    }
    return std::get<QString>(m_id);
}

QTgBot::ChatId::operator QJsonValue() const
{
    if (auto intId = std::get_if<qint64>(&m_id)) {
        return double(*intId);
    }
    return std::get<QString>(m_id);
}

}
