// SPDX-FileCopyrightText: 2021 Linus Jahn <lnj@kaidan.im>
//
// SPDX-License-Identifier: AGPL-3.0-only

#include <QCoreApplication>
#include <QSettings>
#include <QFutureWatcher>

#include "bridgecontroller.h"
#include "qtgbot/api.h"
#include "qtgbot/message.h"

constexpr QStringView APPLICATION_NAME = u"qxbridge";
constexpr QStringView APPLICATION_VERSION = u"0.1.0";
constexpr QStringView ORGANISATION_NAME = u"kaidan";
constexpr QStringView ORGANISATION_DOMAIN = u"kaidan.im";

template<typename T, typename Handler>
void upon(QFuture<T> future, Handler handler)
{
    auto *watcher = new QFutureWatcher<T>();
    QObject::connect(watcher, &QFutureWatcherBase::finished, [watcher, handler { std::move(handler) }]() {
        handler(watcher->result());
        watcher->deleteLater();
    });
    watcher->setFuture(future);
}

template<typename T, typename Success, typename Handler>
void uponSuccess(QFuture<T> future, Handler handler)
{
    upon<T>(std::move(future), [handler { std::move(handler) }](T result) {
        if (auto successResult = std::get_if<Success>(&result)) {
            handler(*successResult);
        } else if (const auto error = std::get_if<QTgBot::Error>(&result)) {
            qDebug() << "Error" << error->description;
        }
//        QCoreApplication::exit();
    });
}

int main(int argc, char *argv[])
{
    QCoreApplication app(argc, argv);
    app.setApplicationName(APPLICATION_NAME.toString());
    app.setApplicationVersion(APPLICATION_VERSION.toString());
    app.setOrganizationName(ORGANISATION_NAME.toString());
    app.setOrganizationDomain(ORGANISATION_DOMAIN.toString());

//    using namespace QTgBot;

//    uponSuccess<UserResult, User>(api.getMe(), [](QTgBot::User user) {
//        qDebug() << user.firstName();
//    });

//    uponSuccess<Result, True>(api.setChatTitle(-1001350000600, "QTgBot test"), [](True) {
//        qDebug() << "Ja geil";
//    });

//    uponSuccess<Result, True>(api.setChatDescription(-1001350000600, "Se bot has changed the description."), [](True) {
//        qDebug() << "Ja geil";
//    });

//    uponSuccess<MessageResult, Message>(api.sendMessage(SendMessage(-1001350000600, "Zieht eucht das mal rein ey: <a href=\"https://www.youtube.com/watch?v=wUbRQWp7KZs\">gates.exe</a>", SendMessage::HTML)), [](QTgBot::Message msg) {
//        qDebug() << msg.messageId();
//    });

    BridgeController controller;


    return app.exec();
}
