// SPDX-FileCopyrightText: 2021 Linus Jahn <lnj@kaidan.im>
//
// SPDX-License-Identifier: AGPL-3.0-only

#include "mucservice.h"

#include "QXmppComponent.h"
#include "QXmppComponentConfig.h"
#include <QXmppUtils.h>
#include <QXmppPresence.h>
#include <QXmppMessage.h>
#include <QXmppVCardIq.h>
#include <QXmppDiscoveryIq.h>

#include <QCryptographicHash>
#include <QFutureWatcher>
#include <QDebug>
#include <QDomElement>
#include <QMimeDatabase>
#include <QMimeType>
#include <QStringBuilder>
#include <QtConcurrentRun>

#include "chatuser.h"
#include "global.h"

constexpr QStringView BOT_USER = u"bot";
constexpr QStringView USER_RESOURCE = u"tg";
constexpr QStringView USER_POSTFIX = u"[tg]";

struct AvatarData
{
    QByteArray data;
    QByteArray hash;
    QMimeType mime;
};

struct MucUser
{
    enum State {
        Disconnected,
        Connecting,
        Connected,
    };

    int uid;
    // local user JID on the component
    QString userJid;
    // room@muc.service/usernick
    QString mucFullJid;
    State state = Disconnected;
    AvatarData avatar;

    QXmppPresence presence() const
    {
        QXmppPresence presence;
        presence.setType(state == Disconnected ? QXmppPresence::Unavailable : QXmppPresence::Available);
        presence.setFrom(userJid);
        presence.setTo(mucFullJid);
        presence.setMucSupported(true);
        if (!avatar.hash.isEmpty()) {
            presence.setVCardUpdateType(QXmppPresence::VCardUpdateValidPhoto);
            presence.setPhotoHash(avatar.hash);
        } else {
            presence.setVCardUpdateType(QXmppPresence::VCardUpdateNotReady);
        }
        return presence;
    }
};

QString fullJid(QStringView user, QStringView domain, QStringView resource)
{
    return user % u'@' % domain % u'/' % resource;
}

class MucServicePrivate
{
public:
    QDateTime startTime = QDateTime::currentDateTimeUtc();
    // MUC room -> (uid -> MucUser)
    QHash<QString, QHash<int, MucUser>> users;
};

MucService::MucService()
    : d(new MucServicePrivate)
{
}

MucService::~MucService()
{
}

bool MucService::handleStanza(const QDomElement &stanza)
{
    if (stanza.tagName() == u"iq" && stanza.attribute("type") == u"get") {
        if (QXmppDiscoveryIq::isDiscoveryIq(stanza)) {
            QXmppDiscoveryIq request;
            request.parse(stanza);

            if (auto *user = findUser(request.to())) {
                QXmppDiscoveryIq iq;
                iq.setId(request.id());
                iq.setType(QXmppIq::Result);
                iq.setFrom(request.to());
                iq.setTo(request.from());
                iq.setFeatures({
                    "vcard-temp",
                });
                component()->sendPacket(iq);
                return true;
            }
            QXmppDiscoveryIq iq;
            iq.setId(request.id());
            iq.setFrom(request.to());
            iq.setTo(request.from());
            iq.setType(QXmppIq::Error);
            iq.setError(QXmppStanza::Error(
                            QXmppStanza::Error::Type::Cancel,
                            QXmppStanza::Error::Condition::ItemNotFound,
                            "Couldn't find such a user."));
            component()->sendPacket(iq);
            return true;
        }

        if (QXmppVCardIq::isVCard(stanza)) {
            QXmppVCardIq request;
            request.parse(stanza);

            if (auto *user = findUser(request.to())) {
                QXmppVCardIq iq;
                iq.setId(request.id());
                iq.setFrom(user->userJid);
                iq.setTo(request.from());
                iq.setType(QXmppIq::Result);
                if (!user->avatar.data.isEmpty()) {
                    iq.setPhoto(user->avatar.data);
                    iq.setPhotoType(user->avatar.mime.name());
                }
                component()->sendPacket(iq);
                qDebug() << "Sent VCARD";
                return true;
            }

            QXmppVCardIq iq;
            iq.setId(request.id());
            iq.setFrom(request.to());
            iq.setTo(request.from());
            iq.setType(QXmppIq::Error);
            iq.setError(QXmppStanza::Error(
                            QXmppStanza::Error::Type::Cancel,
                            QXmppStanza::Error::Condition::ItemNotFound,
                            "Couldn't find such a user."));
            component()->sendPacket(iq);
            qDebug() << "Sent VCARD error: Not found";
            return true;
        }
    }
    return false;
}

void MucService::addMember(const QString &mucJid, const ChatUser &user)
{
    auto &mucUsers = d->users[mucJid];
    const auto mucNick = usernameToMucNick(user.nickname);
    qDebug() << "[MucServer] Join:" << mucJid << mucNick;
    mucUsers.insert(user.uid, {
        user.uid,
        fullJid(user.protocolId, component()->configuration().componentName(), USER_RESOURCE),
        mucJid % u'/' % mucNick,
        MucUser::Disconnected,
        AvatarData()
    });
    connectMucUser(mucUsers[user.uid]);
}

void MucService::removeMember(const QString &mucJid, int uid)
{
    auto &mucUsers = d->users[mucJid];
    if (mucUsers.contains(uid)) {
        disconnectMucUser(mucUsers[uid]);
        mucUsers.remove(uid);
    }
}

void MucService::sendMessage(const QString &mucJid, int uid, const QString &text)
{
    auto &mucUsers = d->users[mucJid];
    if (!mucUsers.contains(uid)) {
        return;
    }

    // mentions
    auto body = text;
    for (auto &user : mucUsers) {
        auto tgUsername = QXmppUtils::jidToResource(user.mucFullJid);
        tgUsername.chop(USER_POSTFIX.size());
        body.replace(u'@' + tgUsername, tgUsername % USER_POSTFIX);
    }

    QXmppMessage msg;
    msg.setType(QXmppMessage::GroupChat);
    msg.setFrom(mucUsers[uid].userJid);
    msg.setTo(mucJid);
    msg.setBody(body);
    component()->sendPacket(msg);
}

void MucService::updateMemberPhoto(const QString &mucJid, int uid, const QByteArray &data)
{
    struct AvatarMeta {
        QByteArray hash;
        QMimeType mime;
    };

    auto hashFuture = QtConcurrent::run([=]() -> AvatarMeta {
        return {
            QCryptographicHash::hash(data, QCryptographicHash::Sha1),
            QMimeDatabase().mimeTypeForData(data),
        };
    });

    await(hashFuture, this, [=](AvatarMeta &&meta) {
        qDebug() << "New avatar set" << meta.mime;
        auto &user = d->users[mucJid][uid];
        user.avatar = AvatarData {
            data,
            meta.hash,
            meta.mime,
        };

        resendPresence(user);
    });
}

void MucService::setComponent(QXmppComponent *component)
{
    QXmppComponentExtension::setComponent(component);
    connect(component, &QXmppComponent::presenceReceived, this, &MucService::onPresenceReceived);
    connect(component, &QXmppComponent::messageReceived, this, &MucService::onMessageReceived);
    connect(component, &QXmppComponent::connected, this, &MucService::onConnected);
}

void MucService::onConnected()
{
//    for (auto &muc : d->users) {
//        for (auto &user : muc) {
//            connectMucUser(user);

//            QXmppPresence presence;
//            presence.setType(QXmppPresence::Available);
//            presence.setFrom(localUserFullJid(BOT_USER));
//            presence.setTo(BRIDGE_MUC.toString() % u'/' % USER_RESOURCE);
//            presence.setMucSupported(true);

//            component()->sendPacket(presence);
//        }
//    }
}

void MucService::onMessageReceived(const QXmppMessage &message)
{
    if (message.body().isEmpty()) {
        return;
    }

    const auto mucJid = QXmppUtils::jidToBareJid(message.from());
    if (!d->users.contains(mucJid)) {
        return;
    }
    auto &bridgedUsers = d->users[mucJid];
    if (bridgedUsers.isEmpty()) {
        return;
    }
    auto &firstUser = bridgedUsers.begin().value();
    if (message.to() != firstUser.userJid) {
        return;
    }

    // ignore MUC history
    if (message.stamp().isValid() && message.stamp().toUTC() <= d->startTime) {
        return;
    }

    // ignore messages from bridged users (our own messages)
    for (auto &user : bridgedUsers) {
        if (user.mucFullJid == message.from()) {
            return;
        }
    }

    // mentions
    auto body = message.body();
    for (auto &user : bridgedUsers) {
        auto mucNick = QXmppUtils::jidToResource(user.mucFullJid);
        auto tgUsername = u'@' + mucNick;
        tgUsername.chop(USER_POSTFIX.size());
        body = body.replace(mucNick, tgUsername);
    }

    emit messageReceived(QXmppUtils::jidToResource(message.from()), body);
}

void MucService::onPresenceReceived(const QXmppPresence &presence)
{
    // check for presences from the MUC itself (no resource)
    if (d->users.contains(presence.from())) {
        auto &mucUsers = d->users[presence.from()];

        for (auto &mucUser : mucUsers) {
            if (mucUser.userJid == presence.to()) {
                if (presence.type() == QXmppPresence::Type::Error) {
                    mucUser.state = MucUser::Disconnected;
                    // todo handle this (check error: nickname conflict ?)
                } else {
                    mucUser.state = MucUser::Connected;
                }
                break;
            }
        }
    }
}

void MucService::connectMucUser(MucUser &user)
{
    user.state = MucUser::Connecting;
    component()->sendPacket(user.presence());
}

void MucService::disconnectMucUser(MucUser &user)
{
    user.state = MucUser::Disconnected;
    component()->sendPacket(user.presence());
}

void MucService::resendPresence(const MucUser &user)
{
    component()->sendPacket(user.presence());
}

MucUser *MucService::findUser(const QString &jid)
{
    const auto fullJid = QXmppUtils::jidToBareJid(jid) % u'/' % USER_RESOURCE;
    for (auto &mucUsers : d->users) {
        for (auto &user : mucUsers) {
            if (fullJid == user.userJid) {
                return &user;
            }
        }
    }
    return nullptr;
}

QString MucService::localUserFullJid(QStringView user)
{
    return user % u'@' % component()->configuration().componentName() % u'/' % USER_RESOURCE;
}

QString MucService::usernameToMucNick(const QString &username)
{
    return username % USER_POSTFIX;
}
