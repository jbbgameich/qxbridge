// SPDX-FileCopyrightText: 2021 Linus Jahn <lnj@kaidan.im>
//
// SPDX-License-Identifier: AGPL-3.0-only

#pragma once

#include <memory>
#include <QObject>

class QTimer;

namespace QTgBot {
class Api;
class Update;
class Message;
}

class TgBotClientPrivate;
class DbTgUser;
class ChatUser;
class Database;

class TgBotClient : public QObject
{
    Q_OBJECT

public:
    TgBotClient(Database *database, QObject *parent = nullptr);
    ~TgBotClient();

    void start();
    void stop();

    QTgBot::Api *api() const;

    QHash<qint64, QVector<ChatUser>> currentMembers();

    Q_SIGNAL void messageReceived(qint64 tgChatId, int uid, const QString &text);
    Q_SIGNAL void memberAdded(qint64 tgChatId, const ChatUser &user);
    Q_SIGNAL void memberRemoved(qint64 tgChatId, int uid);
    Q_SIGNAL void memberPhotoChanged(qint64 tgChatId, int uid, const QByteArray &data);

private:
    Q_SLOT void onUpdateTimer();
    void handleUpdate(const QTgBot::Update &update);
    void initialize();
    void initializeMembers(qint64 chatId);
    void onInitialized();
    void addMember(const DbTgUser &member);
    bool hasMember(qint64 chatId, qint64 userId);
    int memberUid(qint64 chatId, qint64 userId);

    friend class TgBotClientPrivate;

    std::unique_ptr<TgBotClientPrivate> d;
};
