// SPDX-FileCopyrightText: 2021 Linus Jahn <lnj@kaidan.im>
//
// SPDX-License-Identifier: AGPL-3.0-only

#pragma once

#include <memory>
#include <QDateTime>
#include "QXmppComponentExtension.h"

class QXmppPresence;
class QXmppMessage;

class MucServicePrivate;
class ChatUser;
struct MucUser;

class MucService : public QXmppComponentExtension
{
    Q_OBJECT
public:
    MucService();
    ~MucService();

    bool handleStanza(const QDomElement &stanza) override;

    void addMember(const QString &mucJid, const ChatUser &user);
    void removeMember(const QString &mucJid, int uid);
    void sendMessage(const QString &mucJid, int uid, const QString &text);
    void updateMemberPhoto(const QString &mucJid, int uid, const QByteArray &data);
    Q_SIGNAL void messageReceived(const QString &user, const QString &text);

protected:
    void setComponent(QXmppComponent *component) override;

private:
    Q_SLOT void onConnected();
    Q_SLOT void onMessageReceived(const QXmppMessage &message);
    Q_SLOT void onPresenceReceived(const QXmppPresence &presence);

    void connectMucUser(MucUser &user);
    void disconnectMucUser(MucUser &user);
    void resendPresence(const MucUser &user);

    MucUser *findUser(const QString &jid);

    QString localUserFullJid(QStringView user);
    QString usernameToMucNick(const QString &username);

    std::unique_ptr<MucServicePrivate> d;
};
