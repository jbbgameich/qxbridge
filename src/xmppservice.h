// SPDX-FileCopyrightText: 2021 Linus Jahn <lnj@kaidan.im>
//
// SPDX-License-Identifier: AGPL-3.0-only

#pragma once

#include "QXmppComponent.h"

class MucService;

class XmppService : public QXmppComponent
{
public:
    XmppService(QObject *parent = nullptr);

    void connectToServer();

    MucService *mucService() const;

private:
    MucService *m_mucService;
};
